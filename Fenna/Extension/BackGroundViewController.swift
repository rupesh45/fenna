//
//  BackGroundViewController.swift
//  Fenna
//
//  Created by Rupesh Kadam on 13/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit

class BackGroundViewController: UIViewController {
    
    var schoolSelectionModel : SchoolSelectionModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
 
        self.setupTabbarAndNavBar()
        
        
        schoolSelectionModel = SchoolSelectionModel.sharedInstance
        schoolSelectionModel.SetupDesignBack(view: self.view)
    }
    
}

class PlaneBackgroundViewController : UIViewController {
    
    var schoolSelectionModel : SchoolSelectionModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
//        self.setupTabbarAndNavBar()
        
        
        schoolSelectionModel = SchoolSelectionModel.sharedInstance
        schoolSelectionModel.SetupDesignBack(view: self.view)
    }
    
}




