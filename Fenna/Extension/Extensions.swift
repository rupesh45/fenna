//
//  Extensions.swift
//  Fenna
//
//  Created by Rupesh Kadam on 14/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import NVActivityIndicatorView
import Charts

func errorAlert(title : String, message : String,vc : UIViewController){
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
    }))
    vc.present(alert, animated: true)
}

func AlertWithCompletion(title : String,message : String,vc : UIViewController,completion : ((UIAlertAction) -> Void)?) {
    
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: completion))
    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
    vc.present(alert, animated: true)
}

extension NVActivityIndicatorView {
    func setupActivityIndicator(){
        self.type = .ballSpinFadeLoader
        self.color = UIColor.white
    }
}
// image extension
extension UIImage {
    /// Save PNG in the Documents directory
    func save(_ name: String)->URL {
        let path: String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let url = URL(fileURLWithPath: path).appendingPathComponent(name)
//        try! UIImageJPEGRepresentation(self,0.4)?.write(to: url)
        do {
            try self.jpegData(compressionQuality: 0.4)?.write(to: url)
        }catch{
            print("error")
        }
        

        return url
    }
    
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in:UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: image!.cgImage!)
    }
    
    
}

func ReturnImage(_ imageName : String) -> UIImage
{
    
    if(imageName.isEmpty)
    {
        return #imageLiteral(resourceName: "selectuser")
    }
    else
    {
        var image = UIImage()
        
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(imageName)
            print(imageURL.path)
            image = UIImage(contentsOfFile: imageURL.path)!
            // Do whatever you want with the image
            
        }
        
        
        return image
    }
}

//color extensions
extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

extension UIImageView {
    
    func roundCorners() {
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.masksToBounds = true
    }
    
    func roundCornersWith(radius : CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func shareOnInstagramStories() {
        
        if let storiesUrl = URL(string: "instagram-stories://share") {
            
            if UIApplication.shared.canOpenURL(storiesUrl){
                guard let image = self.image else { return }
                guard let imageData = image.pngData() else { return }
                let url = "https://finnatime.com"
                let pasteBoardItems : [String: Any] = [
                    
                    "com.instagram.sharedSticker.stickerImage" : imageData,
                    "com.instagram.sharedSticker.contentURL" : url,
                    "com.instagram.sharedSticker.backgroundTopColor" : "#e0e0e0",
                    "com.instagram.sharedSticker.backgroundBottomColor" : "#ffffff"
                    
                    
                ]
                let pasteBoardOptions = [
                    
                    UIPasteboard.OptionsKey.expirationDate: Date().addingTimeInterval(300)
                ]
                
                UIPasteboard.general.setItems([pasteBoardItems], options: pasteBoardOptions)
                UIApplication.shared.open(storiesUrl, options: [:], completionHandler: nil)
            }else{
                print("Instagram doesnt exist on your device")
            }
            
        }
        
    }
    
}

//viewcontroller extensions
extension UIViewController {
    func setupTabbarAndNavBar(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        //        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = .white
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = .white
        
        self.tabBarController!.tabBar.layer.borderWidth = 0.0
        self.tabBarController!.tabBar.layer.borderColor = UIColor.clear.cgColor
        self.tabBarController!.tabBar.backgroundColor = UIColor(hexString: "#6611CF")
        self.tabBarController!.tabBar.backgroundImage = UIImage()
        self.tabBarController!.tabBar.itemPositioning = .fill
        self.tabBarController?.tabBar.clipsToBounds = true
    }
    
    func setupNavBar(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        //        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = .white
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = .white
    }
    
}

//increament id func
func incrementID(_ obj : Object.Type) -> Int {
    let realm = AppSharedDetails.getRealmInstance()
//    let realm = try! Realm()
    return (realm.objects(obj.self).max(ofProperty: "user_activity_id") as Int? ?? 0) + 1
}

//textfield textfield
extension UITextField {
    func setupPlaceHoder(placeholder: String,color : UIColor){
        
        self.attributedPlaceholder = NSAttributedString(string: placeholder,attributes: [NSAttributedString.Key.foregroundColor: color])
        
    }
}


//date extension
extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}

//piechart extension
extension PieChartView {
    
    func setupPieChart() {
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        
        self.chartDescription?.text = ""
        
        self.legend.enabled = false
    }
    
}

//tableview extension



extension UITableViewCell {
    
    func layoutCell(){
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        self.selectedBackgroundView = backgroundView
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0))
        contentView.addGradientBackground(firstColor: UIColor(hexString: "#2A61EC"), secondColor: UIColor(hexString: "#324DDB"))
        contentView.layer.cornerRadius = 8
        contentView.layer.masksToBounds = true
        
    }

}

//collection extension
extension UICollectionView {
    func registerNib(nibName : String,identifier : String){
        self.register(UINib(nibName: nibName, bundle: nil), forCellWithReuseIdentifier: identifier)
    }
}

extension UITableView {
    func registerNib(nibName : String,identifier : String) {
        self.register(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: identifier)
    }
}

//string extension
extension String {
    func matches(_ regex: String) -> Bool {
        return self.range(of: regex, options: .regularExpression, range: nil, locale: nil) != nil
    }
    
 
    func capitalizingFirstLetter() -> String {
            return prefix(1).uppercased() + self.lowercased().dropFirst()
        }
        
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    var westernArabicNumeralsOnly: String {
        let pattern = UnicodeScalar("0")..."9"
        return String(unicodeScalars
            .compactMap { pattern ~= $0 ? Character($0) : nil })
    }
    
}

//uiview extensions
extension UIView {
    func addGradientBackground(firstColor: UIColor, secondColor: UIColor){
        clipsToBounds = true
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
        gradientLayer.frame = self.bounds
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func setGradientBackgroundButton(view : UIView) {
        let colorTop =  UIColor(hexString: "#FBD15B").cgColor
        let colorBottom = UIColor(hexString: "#F7711A").cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = view.bounds
        
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func animShow(){
        UIView.transition(with: self, duration: 0.5, options: [.transitionFlipFromRight],
                          animations: {
                            self.isHidden = false
                            self.layoutIfNeeded()
        }, completion: nil)
        
    }
    
    func addShadow(){
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 5
    }
    
    func setThemeBackground(){
        let colorTop =  UIColor(hexString: "#2074FC").cgColor
        let colorBottom = UIColor(hexString: "#690ECE").cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.bounds
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width + 1, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    
    
    
}

extension UIButton {
    func underlineMyText() {
        guard let text = self.titleLabel?.text else { return }
        
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        
        self.setAttributedTitle(attributedString, for: .normal)
    }
}


extension UISearchBar
{
    func setPlaceholderTextColorTo(color: UIColor)
    {
        let textFieldInsideSearchBar = self.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = color
        let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = color
    }
    
    
    func setMagnifyingGlassColorTo(color: UIColor)
    {
        let textFieldInsideSearchBar = self.value(forKey: "searchField") as? UITextField
        let glassIconView = textFieldInsideSearchBar?.leftView as? UIImageView
        glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
        glassIconView?.tintColor = color
    }
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func random() -> UIColor {
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}

extension Notification.Name {
    static let reloadTable = Notification.Name("reloadtable")
    static let startActivity = Notification.Name("startActivity")
    static let endActivity = Notification.Name("endActivity")
    static let refreshTimeline = Notification.Name("refreshTimeline")
    static let refreshAnalytics = Notification.Name("refreshAnalytics")
    static let hideFollowAndAnalytics = Notification.Name("hideFollowAndAnalytics")
}

