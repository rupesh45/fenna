//
//  TimelineViewController.swift
//  Fenna
//
//  Created by Rupesh Kadam on 13/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire

class TimelineViewController: BackGroundViewController {

    @IBOutlet weak var TimelineTableView: UITableView!
    @IBOutlet weak var FeedButton: UIButton!
    
    
    var timelineModel : TimelineViewModel!
    var refreshControl = UIRefreshControl()
    
    var timer = Timer()
    let realm = AppSharedDetails.getRealmInstance()
    
    var indexPathArray = [IndexPath]()
    
    var timelineTable : Results<TimelineTable>!
    
    var endActivityId = Int()
    
    var followTable : Results<FollowerTable>!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        timelineTable = realm.objects(TimelineTable.self)
        followTable = realm.objects(FollowerTable.self)
        
        
        // Do any additional setup after loading the view.
        TimelineTableView.delegate = self
        TimelineTableView.dataSource = self
        TimelineTableView.insetsContentViewsToSafeArea = true
        
        TimelineTableView.registerNib(nibName: "TimelineTableViewCell", identifier: "cell")
        
        getActs()
        
    
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        TimelineTableView.refreshControl = refreshControl
        
        NotificationCenter.default.addObserver(self, selector: #selector(onActivityStarted(notification:)), name: Notification.Name.startActivity, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onActivityEnded(notification:)), name: Notification.Name.endActivity, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshTimeline(notification:)), name: Notification.Name.refreshTimeline, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        endTimer()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        getActs()
        self.indexPathArray = []
        self.indexPathArray = self.TimelineTableView.indexPathsForVisibleRows ?? []
        BeginTimer()
    }
    
    
    @objc func refreshTimeline(notification : Notification){
        
        getActs()
        
    }
    
    @objc func onActivityStarted(notification:Notification){
        

        if let liveActivity = notification.userInfo as? [String : Any] {
            print(liveActivity)
            if let id = liveActivity["id"] as? Int,
                let userid = liveActivity["user_id"] as? String,
                let userName = liveActivity["user_name"] as? String,
                let activityName = liveActivity["activity_name"] as? String,
                let school = liveActivity["school"] as? String,
                let grade = liveActivity["grade"] as? String,
                let startTime = liveActivity["start_time"] as? Double,
                let endTime = liveActivity["end_time"] as? Double,
                let preDuration = liveActivity["pre_duration"] as? Int,
                let preEndtime = liveActivity["pre_endtime2"] as? Double,
                let liked = liveActivity["liked"] as? Bool,
                let followCount = liveActivity["follow_count"] as? Int
            {
                
                if let userImage = liveActivity["user_image"] as? String{
                    
                    let timelinePush = TimelineModel.TimeLineResponse(id: id, user_id: userid, user_image: userImage, user_image_url: nil, user_name: userName, school: school, grade: grade, activity_name: activityName, start_time: startTime, end_time: endTime, pre_duration: preDuration, pre_endtime2: preEndtime, liked: liked,follow_count: followCount)
                        timelineModel.liveActivityData.insert(timelinePush, at: 0)
                        TimelineTableView.reloadData()
//                    print(timelinePush)
                    
                }else if let userImageUrl = liveActivity["user_image_url"] as? String{
                    
                    let timelinePush = TimelineModel.TimeLineResponse(id: id, user_id: userid, user_image: nil, user_image_url: userImageUrl, user_name: userName, school: school, grade: grade, activity_name: activityName, start_time: startTime, end_time: endTime, pre_duration: preDuration, pre_endtime2: preEndtime, liked: liked,follow_count: followCount)
                    
                    timelineModel.liveActivityData.insert(timelinePush, at: 0)
                    TimelineTableView.reloadData()
                }
                
                
                
            }
        
            
        }
        
    }
    
    @objc func onActivityEnded(notification: Notification) {
        
        if let liveActivity = notification.userInfo as? [String : Any] {
            getActs()
        }
    }
    
    func getActs(){
        timelineModel.getLiveActivity { (complete) in
            if complete {
                print("complete")
                self.endTimer()
                self.indexPathArray = []
                self.indexPathArray = self.TimelineTableView.indexPathsForVisibleRows ?? []
                self.TimelineTableView.reloadData()
                self.indexPathArray = self.TimelineTableView.indexPathsForVisibleRows ?? []
                self.BeginTimer()
                
            }else{
                print("response error")
            }
        }
    }
    
//    func followUser(followeId : String,completion : @escaping (Bool) -> Void){
//        timelineModel.followUserWith(followerId: followeId) { (bool) in
//            if bool {
//                print("followed")
//                completion(true)
//            }else{
//                print("serer response error")
//            }
//        }
//    }
    
    func likeActivity(userid : String,activityid : Int,completion : @escaping (Bool) -> Void) {
        
        timelineModel.likeActivity(userId: userid, activityId: activityid) { (bool) in
            if bool {
                print("liked")
                completion(true)
            }else{
                print("serer response error")
            }
        }
        
    }
    
//    @objc func OnFollowButtonTapped(_ sender : UIButton){
//
//
//        if sender.titleLabel?.text == "follow"{
//            let userId = timelineModel.getUserId(index: sender.tag)
//            print(userId)
//            followUser(followeId: userId) { (bool) in
//                if bool {
//                    sender.setTitle("unfollow", for: .normal)
//                }else{
//                    print("response error")
//                }
//            }
//        }else{
//
//            //unfollow here
//            timelineModel.unfollowUser { (bool) in
//                if bool {
//                    sender.setTitle("follow", for: .normal)
//                }else{
//                    print("response error")
//                }
//            }
//
//        }
//
//
//
//    }
    
    @objc func OnLikeButtonTapped(_ sender : UIButton) {
        
        let userId = timelineModel.getUserId(index: sender.tag)
        print(userId)
        let activityId = timelineModel.getActivityId(index: sender.tag)
        print(activityId)
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell  = self.TimelineTableView.cellForRow(at: indexPath) as! TimelineTableViewCell
        if timelineModel.liveActivityData[indexPath.row].liked == false {
            
            likeActivity(userid: userId, activityid: activityId) { (bool) in
                if bool {
                    self.timelineModel.liveActivityData[indexPath.row].liked = true
                    cell.LikeButton.setBackgroundImage(UIImage(named: "likedheart")!, for: .normal)
                    self.TimelineTableView.reloadRows(at: [indexPath], with: .automatic)
                    print("liked succes")
                }else{
                    print("like error")
                }
            }
            
        }else{
            timelineModel.unlikeActivity(userid : userId,activityId: activityId) { (bool) in
                if bool {
                    self.timelineModel.liveActivityData[indexPath.row].liked = false
                    cell.LikeButton.setBackgroundImage(UIImage(named: "heart")!, for: .normal)
                    self.TimelineTableView.reloadRows(at: [indexPath], with: .automatic)
                    print("Unliked")
                }else{
                    print("unlike error")
                }
            }
        }
        

        
        
        
    }
    
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "feed" {
            if let vc = segue.destination as? ActivityFeedViewController{
                vc.timelineModel = timelineModel
            }
        }else if segue.identifier == "userprofile" {
            if let vc = segue.destination as? UserProfileViewController {
                
                vc.timelinemodel = self.timelineModel
                
                if let indexpath = TimelineTableView.indexPathForSelectedRow{
                    
                    vc.follCountFromTimeline = timelineModel.liveActivityData[indexpath.row].follow_count
                    vc.userId = timelineModel.liveActivityData[indexpath.row].user_id
                    vc.userName = timelineModel.liveActivityData[indexpath.row].user_name
                    vc.schoolAndGrade = timelineModel.liveActivityData[indexpath.row].school + " - " + timelineModel.liveActivityData[indexpath.row].grade
                    timelineModel.getTimelineImage(index: indexpath.row) { (image) in
                        vc.UserProfileImageView.image = image
                    }
                }
                
                
            }
        }
    }
    
    @objc func onRefresh(refreshControl: UIRefreshControl) {

        
        timelineModel.getLiveActivity { (complete) in
            if complete {
                self.getActs()

                refreshControl.endRefreshing()
            }else{
                print("response error")
                refreshControl.endRefreshing()
            }
        }

        // somewhere in your code you might need to call:

    }
    

    
    @IBAction func onFeedButtonTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "feed", sender: nil)
    }
    

    
    func BeginTimer() {
//        self.index = index
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    
    
     @objc func updateTime() {
        

        
        for indexpath in indexPathArray
        {
            
          
            if let cell = TimelineTableView.cellForRow(at: indexpath) as? TimelineTableViewCell
            {
            
                if timelineModel.liveActivityData[indexpath.row].pre_duration == 0 {
                    if Date() < timelineModel.epochToDate(index: indexpath.row){
                       cell.UserActivityTimeLeftLabel.text = timelineModel.timeRemainingString(finishDate: timelineModel.epochToDate(index: indexpath.row))
                    }else{
                        cell.UserActivityTimeLeftLabel.text = "Completed."
                    }
                    
                }else if timelineModel.liveActivityData[indexpath.row].pre_duration >= 1 {
                    cell.UserActivityTimeLeftLabel.text = "Ended."
                }else{
                    cell.UserActivityTimeLeftLabel.text = "Completed."
                }
                
                
            }
        }
        
        
        
    }
    
    func endTimer() {
        
        timer.invalidate()
        
    }
    

}

extension TimelineViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timelineModel.getTimelineCount()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TimelineTableViewCell
        
        
        cell.tag = indexPath.row
        cell.timelineModel = timelineModel
        
        cell.FollowButton.tag = indexPath.row
        cell.LikeButton.tag =  indexPath.row

        if timelineModel.liveActivityData[indexPath.row].liked == false {
            cell.LikeButton.setBackgroundImage(UIImage(named: "heart")!, for: .normal)
        }else{
            cell.LikeButton.setBackgroundImage(UIImage(named: "likedheart")!, for: .normal)
        }
        
//        cell.FollowButton.addTarget(self, action: #selector(OnFollowButtonTapped(_:)), for: .touchUpInside)
        cell.LikeButton.addTarget(self, action: #selector(OnLikeButtonTapped(_:)), for: .touchUpInside)
        
        if timelineModel.liveActivityData[indexPath.row].pre_duration == 0 {
            if Date() < timelineModel.epochToDate(index: indexPath.row){
                cell.UserActivityTimeLeftLabel.text = timelineModel.timeRemainingString(finishDate: timelineModel.epochToDate(index: indexPath.row))
            }else{
                cell.UserActivityTimeLeftLabel.text = "Completed."
            }
        }else if timelineModel.liveActivityData[indexPath.row].pre_duration >= 1 {
            cell.UserActivityTimeLeftLabel.text = "Ended."
        }else{
            cell.UserActivityTimeLeftLabel.text = "Completed."
        }
        
        cell.UserCurrentActivityLabel.text = timelineModel.getTimelineUserNameAndActivity(index: indexPath.row)
        
//        timelineModel.getTimelineImage(index: indexPath.row) { (image) in
//            cell.UserImageView.image = image
//        }
        
        if let image = timelineModel.liveActivityData[indexPath.row].user_image {
            cell.UserImageView.loadImageFromurl(urlString: image)
        }else if let userImgaeUrl = timelineModel.liveActivityData[indexPath.row].user_image_url {
            cell.UserImageView.loadImageFromurl(urlString: userImgaeUrl)
        }
        

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TimelineTableViewCell
        performSegue(withIdentifier: "userprofile", sender: nil)
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastItem = timelineModel.liveActivityData.count - 1
        if indexPath.row == lastItem {
            print("request more data.")
            loadMoreData()
        }
    }
    
    func loadMoreData() {
        timelineModel.loadMoreData { (bool) in
            if bool {
                self.TimelineTableView.reloadData()
            }else{
                print("response error")
            }
        }
    }


}


