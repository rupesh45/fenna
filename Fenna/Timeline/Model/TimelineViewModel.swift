//
//  TimelineModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 13/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireImage
import RealmSwift

class TimelineViewModel {
    
    static let sharedInstance = TimelineViewModel()
    
    var timer = Timer()
    var totalSecond = Int()
    
    var liveActivityData = [TimelineModel.TimeLineResponse]()
    var followData : TimelineModel.Follow!
    
    var endDateEpoch = [Double]()
    
//    var liveActivityId = Int()
    let realm = AppSharedDetails.getRealmInstance()
    
    
    var timelinePushData : [[String : Any]] = []
    
    var nextApiString : String?
    
//    var timelineTable : Results<TimelineTable>!
    
//    var followTable : Results<FollowerTable>!
    
    init() {
        
    }
    
    
    func getLiveActivity(completion :@escaping (Bool) -> Void){

//        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.activity + NetworkConstant.SubUrlMethods.user + NetworkConstant.live.live
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.activity + NetworkConstant.SubUrlMethods.user
        
        NetworkModel.sharedInstance.startNetworkGetCallWithHeader(URL: url) { (response) in
            if let status = response.response?.statusCode {
                print(status)
                switch status {
                case 200...300 :
                    print("response success")
                    print(response.result.value)
                    if let data = response.data {
                        do {
                            let json = try JSONDecoder().decode(TimelineModel.MainTimeLine.self, from: data)
                            self.liveActivityData = json.results
//                            self.nextApiString = json.next
//                            print(self.nextApiString)
                            if let nextApi = json.next {
                                self.nextApiString = nextApi
                            }
//
                            
                            
                            completion(true)
                            
                        }catch{
                            print("unable to decode data")
                      
                            completion(false)
                            
                        }
                    }
                    
                case 400...600 :
                
                        completion(false)
                    
                    print("error with status code \(status)")
                    
                default:
          
                        completion(false)
                    
                    print("default \(status)")
                }
                
                
            }
        }
        
    }
    
    func loadMoreData(completion :@escaping (Bool) -> Void) {
        
        if let url = nextApiString {
            
            NetworkModel.sharedInstance.startNetworkGetCallWithHeader(URL: url) { (response) in
                if let status = response.response?.statusCode {
                    print(status)
                    switch status {
                    case 200...300 :
                        print("response success")
                        print(response.result.value)
                        if let data = response.data {
                            do {
                                let json = try JSONDecoder().decode(TimelineModel.MainTimeLine.self, from: data)
                                
                                
                                for data in json.results {
                                    self.liveActivityData.append(data)
                                }
                                
                                if let nextApi = json.next {
                                    self.nextApiString = nextApi
                                }
                                
                                
                                
                                completion(true)
                                
                            }catch{
                                print("unable to decode data")
                                
                                completion(false)
                                
                            }
                        }
                        
                    case 400...600 :
                        
                        completion(false)
                        
                        print("error with status code \(status)")
                        
                    default:
                        
                        completion(false)
                        
                        print("default \(status)")
                    }
                    
                    
                }
            }
            
        }

        
    }
    
    func followUserWith(followerId : String,completion :@escaping (Bool) -> Void) {
        
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.userfollower
        
        let params = Packets()
        print(params.createFollowerPacketWith(followerId: followerId))
        
        NetworkModel.sharedInstance.startNetworkPostCall(withURL: url, andParams: params.createFollowerPacketWith(followerId: followerId)) { (response) in
            if let status = response.response?.statusCode {
                print(status)
                switch status {
                case 200...300:
                    print("response success")
                    print(response.result.value)
                    
                    if let data  = response.data {
                        do{
                            let results = try JSONDecoder().decode(TimelineModel.Follow.self, from: data)
                            print(results)
                            self.followData = results
                            let realmObj = self.realm.objects(FollowerTable.self)
//                            print(realmObj)
                            
                            if realmObj.count == 0 {
                                
                                
                                let obj = FollowerTable()
                                
                                obj.id = results.id
                                obj.my_id = results.follower
                                obj.user_id = results.user
                                obj.isfollowed = results.follow
                                obj.username = results.user_name
                                
                                do{
                                    try self.realm.write {
                                        self.realm.add(obj)
                                        completion(true)
                                    }
                                }catch{
                                    print("unable to add realm object.")
                                    completion(false)
                                }
                                
                            }else{
                                print("already exists")
                                completion(false)
                            }
                            
                            
                        }catch{
                            print("unable to parse")
                            completion(false)
                        }
                        
                    }
                    
                  
                case 400...600:
                    print(response.result.value)
                    print("response error")
                    completion(false)
                default:
                    print("response error default")
                    completion(false)
                }
            }
        }
        
    }
    
    func unfollowUser(completion : @escaping(Bool) -> Void){
        
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.userfollower + "?unfollow=True"
        
        NetworkModel.sharedInstance.startNetworkPostCall(withURL: url, andParams: [:]) { (response) in
            if let status = response.response?.statusCode {
                print(status)
                switch status {
                case 200...300:
                    print("response success")
                    print(response.result.value)
                    let realmObj = self.realm.objects(FollowerTable.self)
//                    print(realmObj)
                    
                    do{
                        try self.realm.write {
                            self.realm.delete(realmObj)
//                            print(self.realm.objects(FollowerTable.self))
                            completion(true)
                        }
                    }catch{
                        print("Unable to delete object.")
                         completion(false)
                    }
                    
//                    completion(true)
                case 400...600:
                    print("response error")
                    completion(false)
                default:
                    print("response error default")
                    completion(false)
                }
            }
        }
        
    }
    
    func likeActivity(userId : String, activityId : Int, completion : @escaping(Bool) -> Void) {
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.activityliked
        
        let paramters = Packets()
        
        NetworkModel.sharedInstance.startNetworkPostCall(withURL: url, andParams: paramters.createLikePacketWith(activityId: activityId, userId: userId)) { (response) in
            if let status = response.response?.statusCode {
                print(status)
                switch status {
                case 200...300:
                    print("response success")
                    print(response.result.value)
                    
                    completion(true)
                case 400...600:
                    print("response error")
                    print(response.result.value)
                    completion(false)
                default:
                    print("response error default")
                    completion(false)
                }
            }
        }
        
        
        
    }
    
    func unlikeActivity(userid : String, activityId : Int,completion : @escaping(Bool) -> Void) {
        
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.activityliked + "?unfollow=True"

        let parameters : Parameters = ["activity" : activityId,
                                       "user_id" : userid]

        NetworkModel.sharedInstance.startNetworkPostCall(withURL: url, andParams: parameters) { (response) in
            if let status = response.response?.statusCode {
                print(status)
                switch status {
                case 200...300:
                    print("response success")
                    print(response.result.value)
                    completion(true)
                case 400...600:
                    print("response error")
                    print(response.result.value)
                    completion(false)
                default:
                    print("response error default")
                    completion(false)
                }
            }
        }
        

        
        
    }
    

    
    func getUserId(index : Int) -> String {
        
        return liveActivityData[index].user_id
    }
    
    func getActivityId(index : Int) -> Int {
        return liveActivityData[index].id
    }
    

    
    func epochToDate(index : Int) -> Date{
        

         let endTime = liveActivityData[index].end_time
        
        let endDate = Date(timeIntervalSince1970: endTime)
        
        return endDate
    }
    
    func timeRemainingString(finishDate date:Date) -> String {
        let secondsFromNowToFinish = date.timeIntervalSinceNow
        let hours = Int(secondsFromNowToFinish / 3600)
        let minutes = Int((secondsFromNowToFinish - Double(hours) * 3600) / 60)
        let seconds = Int(secondsFromNowToFinish - Double(hours) * 3600 - Double(minutes) * 60 + 0.5)
        
        if hours == 1 {
            
            return String(format: "%02d:%02d:%02d",hours, minutes, seconds) + " hour left"
            
        }
        
        if minutes == 0 {
            //            self.minutesLeftLabel.text = "Seconds Left."
            return String(seconds) + " seconds left."
        }else{
            //        print(minutes)
            //        return String(minutes + 1)
            return String(format: "%02d:%02d", minutes, seconds) + " minutes left."
        }
        
        
        
    }
    
    func onActivityStarted(liveActivity : [String : Any],completion : (Bool) -> Void) {
        print(liveActivity)

        if let id = liveActivity["id"] as? Int,
           let userImage = liveActivity["user_image"] as? String,
           let userName = liveActivity["user_name"] as? String,
           let activityName = liveActivity["activity_name"] as? String,
           let startTime = liveActivity["start_time"] as? Double,
           let endTime = liveActivity["end_time"] as? Double,
           let preDuration = liveActivity["pre_duration"] as? Int,
           let preEndtime = liveActivity["pre_endtime2"] as? Double
        {
//            let timelinePush = TimelineModel.TimeLineResponse(id: id, user_image: userImage, user_name: userName, activity_name: activityName, start_time: startTime, end_time: endTime, pre_duration: preDuration, pre_endtime2: preEndtime)
//
////            self.liveActivityData.append(timelinePush)
//            print(self.liveActivityData)

        }
        completion(true)
    }
    
    
    
    //design
    
    let mockTimeLine : [NSDictionary] = [["name" : "rupesh",
                                          "image" : UIImage(named: "sf1")!,
                                         "activity" : "playing piano",
                                         "timeleft" : 10],
                                         
                                         ["name" : "lean",
                                          "image" : UIImage(named: "storefront1")!,
                                          "activity" : "playing cards",
                                          "timeleft" : 15],
                                         
                                         ["name" : "sanket",
                                          "image" : UIImage(named: "university")!,
                                          "activity" : "playing drums",
                                          "timeleft" : 20]]
    
    func ConfigureTimelineCell(button : UIButton,imageView : UIImageView){
        button.layer.cornerRadius = 10
        button.layer.masksToBounds = true
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 1
        
        imageView.layer.cornerRadius = imageView.frame.width / 2
        imageView.layer.masksToBounds =  true
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor = UIColor(hexString: "#FDE793").cgColor
        
    }
    
    
    func getTimelineCount() -> Int {
        print(liveActivityData.count)
        return self.liveActivityData.count
    }
    
//    func getTimelineImage(index : Int) -> UIImage {
//
//        if let img = mockTimeLine[index].value(forKey: "image") as? UIImage {
//            return img
//        }else {
//            return UIImage()
//        }
//
//    }
    
    func getTimelineImage(index : Int,completion : @escaping(UIImage) -> Void) {
        
        if let imgurl = liveActivityData[index].user_image {
            let imgUrlString = imgurl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            if let imgUrl = URL(string: imgUrlString) {
                GetActivityImage(withUrl: imgUrl) { (image) in
                    completion(image)
                }
            }

        }else if let imgurl = liveActivityData[index].user_image_url {
            let imgUrlString = imgurl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            if let imgUrl = URL(string: imgUrlString) {
                GetActivityImage(withUrl: imgUrl) { (image) in
                    completion(image)
                }
            }

        }
        
        
        
    }
    
    
    private func GetActivityImage(withUrl :URL, completion : @escaping(UIImage) -> Void ){
        var activityImage = UIImage()
        Alamofire.request(withUrl).responseImage { (response) in
            if let img = response.result.value {
                activityImage = img
                completion(activityImage)
            }
        }
        
    }
    
    func getTimelineUserNameAndActivity(index : Int) -> String {
        
        var firstname = String()
        let fullName = liveActivityData[index].user_name
        var components = fullName.components(separatedBy: " ")
        if(components.count > 0)
        {
            let firstName = components.removeFirst()
//            let lastName = components.joined(separator: " ")
            firstname = firstName
        }
        
        return firstname + " - " + getActivityName(index: index)

    }
    
    private func getActivityName(index : Int) -> String{
        return liveActivityData[index].activity_name
    }
    
    func getLeftTime(index : Int) -> String {
        if let timeLeft = mockTimeLine[index].value(forKey: "timeleft") as? Int {
            return String(timeLeft)
        }else {
            return ""
        }
    }
    
    func getTimeLeftInt(index : Int) -> Int {
        if let timeLeft = mockTimeLine[index].value(forKey: "timeleft") as? Int {
            return timeLeft
        }else {
            return 0
        }
    }

    
}

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    
    
    
    func loadImageFromurl(urlString : String) {
        let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        var imgString : String?
        
        imgString = urlString
        
        image = nil
        
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageFromCache
            return
        }
        
        GetActivityImage(withUrl: url!) { (img) in
            let imageToCache = img
            
            if imgString == urlString {
                
                self.image = imageToCache
                
            }

            imageCache.setObject(imageToCache, forKey: urlString as AnyObject)
        }
        
        
    }
    
    private func GetActivityImage(withUrl :URL, completion : @escaping(UIImage) -> Void ){
        var activityImage = UIImage()
        Alamofire.request(withUrl).responseImage { (response) in
            if let img = response.result.value {
                activityImage = img
                completion(activityImage)
            }
        }
        
    }
    
}

