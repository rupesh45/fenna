//
//  TimelineModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 24/04/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation

class TimelineModel : Codable {
    
    init() {
        
    }
    
    struct MainTimeLine : Codable{
        let count : Int
        let next : String?
        let results : [TimeLineResponse]
    }
    
    struct TimeLineResponse : Codable {
        
        let id : Int
        let user_id : String
        let user_image : String?
        let user_image_url : String?
        let user_name : String
        let school : String
        let grade : String
        let activity_name : String
        let start_time : Double
        let end_time : Double
        let pre_duration : Int
        let pre_endtime2 : Double
        var liked : Bool
        var follow_count : Int
        
        
    }
    
    struct Follow : Codable {
        let id : Int
        let user :  String
        let follower : String
        let follow : Bool
        let user_name : String
    }
    
    
    
    
}
