//
//  EnterMobileNoViewController.swift
//  Fenna
//
//  Created by Rupesh Kadam on 25/06/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit

class EnterMobileNoViewController: PlaneBackgroundViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var PhoneNumberTextField: UITextField!
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var CountryCodeTextField: UITextField!
    
    
    private var PhoneNumber = ""
    private var otp = ""
    private var countryCode = ""
    
    var entermonModel : EnterMobViewModel!
    
    var userItem : UserItem!
    var schoolSelectionItem : SchoolSelectionItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        entermonModel = EnterMobViewModel()
        
        entermonModel.setupButton(button: NextButton, color: UIColor(hexString: "#8FD4FE"))
//        NextButton.setGradientButton()
        
        PhoneNumberTextField.delegate = self
        PhoneNumberTextField.keyboardType = .phonePad
        PhoneNumberTextField.setupPlaceHoder(placeholder: "", color: UIColor(hexString: "#d8d8d8"))
        PhoneNumberTextField.becomeFirstResponder()
        
        CountryCodeTextField.delegate = self
        CountryCodeTextField.text = "+1"
        countryCode = "+1"
        CountryCodeTextField.keyboardType = .phonePad
        
        print(userItem)
        print(schoolSelectionItem)
        
    }
    
    func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "(XXX) XXX-XXXX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == CountryCodeTextField {
            countryCode = textField.text!
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == PhoneNumberTextField {
            //For mobile numer validation
            
            let allowedCharacters = CharacterSet(charactersIn:"+0123456789 ")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            
            if allowedCharacters.isSuperset(of: characterSet) {
                let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                textField.text = formattedNumber(number: newString)
                return false
            }else{
                return false
            }
        }else{
            let allowedCharacters = CharacterSet(charactersIn:"+0123456789 ")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            
            if allowedCharacters.isSuperset(of: characterSet) {
                let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                textField.text = newString
                return false
            }else{
                return false
            }
        }
        
        
    }
    
    func validation() -> Bool {
        
        if self.PhoneNumberTextField.text!.count < 14 {
            let alert = UIAlertController(title: "Error", message: "Please enter 10 digit phone number", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
            
        }else{
            return true
        }
        
        
        
    }
    
    @IBAction func NextButtonTapped(_ sender: UIButton) {
        
        if validation() {

            if let no = PhoneNumberTextField.text{

                print(no.westernArabicNumeralsOnly)
                self.PhoneNumber = no.westernArabicNumeralsOnly
                
                sendMobNo(mobNo: countryCode + PhoneNumber) { (cmplt,code)  in
                    if cmplt {
                        
                        self.otp = code
                        self.performSegue(withIdentifier: "tootpverification", sender: nil)
                    }else{
                        print("response error")
                    }
                }
                
                

            }
            
            
            
        }else{
            validation()
        }
        
    }
    
    
    func sendMobNo(mobNo : String,complete : @escaping(Bool,String) -> ()) {
        print(mobNo)
        entermonModel.getOtp(mobileNumber: mobNo, completion: complete)
        
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "tootpverification" {
            if let vc = segue.destination as? OtpVerificationViewController {
                vc.entermonModel = self.entermonModel
                vc.phoneNumber = self.PhoneNumber
                vc.otp = self.otp
                vc.countryCode = self.countryCode
//                vc.userItem = self.userItem
                vc.schoolSelectionItem = self.schoolSelectionItem
            }
        }
    }
    
    
    

}
