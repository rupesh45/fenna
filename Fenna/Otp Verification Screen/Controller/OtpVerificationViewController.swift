//
//  OtpVerificationViewController.swift
//  Fenna
//
//  Created by Rupesh Kadam on 25/06/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit
import KWVerificationCodeView
import NVActivityIndicatorView

struct UserItem {
    let name : String
    let schoolId : Int
    let gradeId : Int
    let location : String
    let image : UIImage
    let fcm : String
}

class OtpVerificationViewController: PlaneBackgroundViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var VerifyButton: UIButton!
    @IBOutlet weak var OtpView: KWVerificationCodeView!
    @IBOutlet weak var DummyField: UITextField!
    @IBOutlet weak var OtpLabel: UILabel!
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    
    var entermonModel : EnterMobViewModel!
    var otpVerifyModel : OtpVerifyViewModel!
    
    var phoneNumber = ""
    var otp = ""
    var countryCode = ""
    var fcmToken = AppUserDetails.getFcmToken()
    
    var school = ""
    
    var registrationModel : RegistrationModel!
    
    var userItem : UserItem!
    var schoolSelectionItem : SchoolSelectionItem!
    let userId = AppUserDetails.getUserId()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        entermonModel = EnterMobViewModel()
        otpVerifyModel = OtpVerifyViewModel()
        registrationModel = RegistrationModel.sharedInstance
        
        otpVerifyModel.setDelegate(delegate: self)

        
        entermonModel.setupButton(button: VerifyButton, color: UIColor(hexString: "#8FD4FE"))
        
//        DummyField.becomeFirstResponder()
//        DummyField.keyboardType = .numberPad
        
        OtpView.setCursor()
        OtpView.keyboardType = .numberPad
        
        OtpLabel.text = otp
        print(userItem)
        print(schoolSelectionItem)

    }
    

    
    
    func getCode() -> String {
        
        return OtpView.getVerificationCode()
        
    }
    
    
    
    @IBAction func OnVerifyButtonTapped(_ sender: UIButton) {
        
        
        
        
        if OtpView.hasValidCode() {
            print(getCode())
            print(phoneNumber)
            print(self.fcmToken)
            verifyOtp(otp: getCode(), phoneNumber: countryCode + phoneNumber, fcm_token: fcmToken, otpVerify: { (bool) in
                
                
                if bool {
                    print("otp matched")
                }else{
                    print("wrong otp")
                    errorAlert(title: "Error", message: "Please enter correct otp", vc: self)
                }
                
                
            }) { (cmplt) in
                
                
                if cmplt{
                    print("success")
                    // save user here
                    let defaults = AppUserDetails()
                    
                    print(AppUserDetails.getUserId())
                    print(self.school)
                    
                    if self.school.count > 1 {
                        //already registerd go ahead

//                        defaults.setUserUpdate(userId: AppUserDetails.getUserId())
                        
                        //patch here
                        self.UpdateUser()
//                        self.performSegue(withIdentifier: "mainscreen", sender: nil)
                        
                    }else{
                        print(self.phoneNumber)
//                        defaults.setMobileVerified(mobile: self.phoneNumber)
                        //new user
//                        self.registerUser()
                        self.performSegue(withIdentifier: "createprof", sender: nil)
                        
//                        self.performSegue(withIdentifier: "home", sender: nil)
                    }
                    
                    
                    
                    
                    
                }else{
                    print("response error")
                    errorAlert(title: "Error", message: "Response error", vc: self)
                }
                
                
            }
        }else{
            print("Please enter 4 digit otp.")
        }
        

        
        
        
    }
    
    func UpdateUser() {

        otpVerifyModel.updateUserWith(location: schoolSelectionItem.location, userId: AppUserDetails.getPreviousUserId(), schoolId: schoolSelectionItem.schoolId, gradeId: schoolSelectionItem.gradeId) { (complete) in
            if complete {
                print("complete")
                
                self.performSegue(withIdentifier: "mainscreen", sender: nil)
                
            }else{
                print("server error")
                errorAlert(title: "Error", message: "Response error", vc: self)
            }
        }
        
    }
    
    func registerUser(){
        self.VerifyButton.isEnabled = false
        
         let firstname = userItem.name
         let lastname = ""
         let image = userItem.image
        
        print(userItem)
            
            registrationModel.registerUserWith(firstName: firstname, lastName: lastname, schoolId: self.userItem.schoolId,gradeId : self.userItem.gradeId, location: self.userItem.location,userImage: image, token: userItem.fcm) { (bool) in
                if bool {
                    print("completion success")
                    self.activityIndicator.stopAnimating()
                    
                    self.performSegue(withIdentifier: "createprof", sender: nil)
                    
                }else{
                    self.activityIndicator.stopAnimating()
                    errorAlert(title: "", message: "server error", vc: self)
                    self.VerifyButton.isEnabled = true
                    print("registration error")
                }
                
            }
            
        
        
    }
    
    func verifyOtp(otp : String,phoneNumber : String,fcm_token : String,otpVerify : @escaping(Bool) ->(),completion : @escaping(Bool) -> ()){
        
        otpVerifyModel.VerifyOtpWith(otp: otp, phoneNumber: phoneNumber, fcm_token: fcm_token, otpVerify: otpVerify, completion: completion)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "createprof" {

            if let vc = segue.destination as? RegistrationViewController {

                vc.schoolSelectionItem = self.schoolSelectionItem

            }

        }else if segue.identifier == "mainscreen" {
            if let Tvc = segue.destination as? UITabBarController {
                //tab 1
                if let Nvc = Tvc.viewControllers!.first as? UINavigationController {
                    if let vc = Nvc.viewControllers.first as? NewActivityViewController {
                        vc.activityModel = MainActivityModel.sharedInstance
                        vc.TempSchoolSelectionModel = SchoolSelectionModel.sharedInstance
                        vc.analysisModel = AnalysisViewModel.sharedInstance

                    }
                }
                //tab 2
                if let Nvc = Tvc.viewControllers![1] as? UINavigationController {
                    if let vc = Nvc.viewControllers.first as? TimelineViewController {
                        vc.timelineModel = TimelineViewModel()

                    }
                }

                //tab3
                if let Nvc = Tvc.viewControllers![2] as? UINavigationController {
                    if let vc = Nvc.viewControllers.first as? AnalysisViewController {
                        //                        vc.timelineModel = TimelineModel()


                        vc.analysisModel = AnalysisViewModel.sharedInstance

                    }
                }

                //tab 4
                if let Nvc = Tvc.viewControllers![3] as? UINavigationController {
                    if let vc = Nvc.viewControllers.first as? MyProfileViewController {
                        //                        vc.timelineModel = TimelineModel()


                        vc.myProfileModel = MyProfileViewModel()

                    }
                }


            }
        }
    }
    

}

extension OtpVerificationViewController : PhoneNumberAlreadyRegisterdProtocol {
    
    func sendSchoolInfo(school: String) {
        
        print(school)
        self.school = school
        
    }
    
    
}
