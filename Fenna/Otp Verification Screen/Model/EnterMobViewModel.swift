//
//  EnterMobViewModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 25/06/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import UIKit

class EnterMobViewModel {
    
    init() {
        
    }
    
    func getOtp(mobileNumber : String,completion : @escaping(Bool,String) -> ()){
        
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.users
        
        NetworkModel.sharedInstance.startNetworkPostCallNoHeader(withURL: url, andParams: ["phone_number": mobileNumber]) { (response) in
            if let status = response.response?.statusCode {
                print(status)
                print(response.result.value)
                switch status{
                case 200...300:
                    print("response success")
                    if let data = response.data {
                        
                        
                        do{
                            
                            let result = try JSONDecoder().decode(EnterMobModel.PhoneNumberResponse.self, from: data)
                            print(result)
                            
                            
                            
                            
                            
                            completion(true,result.verify_code)
                            
                        }catch{
                            print("unable to parse")
                            completion(false,"unable to parse")
                        }
                        
                        
                    }
                    
                    
                case 400...600:
                    print("response error")
                    
                    completion(false,"response error")
                    
                default:
                    print("response error")
                    completion(false,"response error")
                }
                
            }
        
    
        }
        
    }
    
    //design here
    
    func setupButton(button : UIButton,color : UIColor ){
        button.layer.cornerRadius = button.frame.height / 2
        button.layer.masksToBounds = true
//        button.backgroundColor = color
        button.setGradientButton()
    }
    
}
