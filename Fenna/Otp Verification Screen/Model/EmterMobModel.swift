//
//  EmterMobModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 26/06/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation

class EnterMobModel {
    
    struct PhoneNumberResponse : Codable {
        let phone_number : String
        let verify_code : String
    }
    
    
}
