//
//  OtpVerifyViewModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 26/06/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import Alamofire

protocol PhoneNumberAlreadyRegisterdProtocol : class {
    func sendSchoolInfo(school : String)
}

class OtpVerifyViewModel {
    
    init() {
        
    }
    
    var otpVerify : Bool?
    var token = ""
    var userId = ""
    weak var delegate : PhoneNumberAlreadyRegisterdProtocol?
    
    func setDelegate(delegate : PhoneNumberAlreadyRegisterdProtocol) {
        self.delegate = delegate
    }
    
    func VerifyOtpWith(otp : String,phoneNumber : String,fcm_token : String,otpVerify : @escaping(Bool) ->(), completion : @escaping(Bool) -> ()){
        
        
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.userverify
        
        
        let paramters : Parameters = ["phone_number" : phoneNumber,
                                      "verify_code" : otp,
                                      "fcm_token" : fcm_token]
        
        NetworkModel.sharedInstance.startNetworkPostCallNoHeader(withURL: url, andParams: paramters) { (response) in
            if let status = response.response?.statusCode {
                print(status)
                print(response.result.value)
                switch status{
                case 200...300:
                    print("response success")
                    if let data = response.data {
                        
                        
                        do{
                            
                            let result = try JSONDecoder().decode(OtpVerifyModel.OtpVerifyResponse.self, from: data)
                            print(result)
                            
                            
                            
                            
                            if let name = result.first_name {
                                print(name)
                                
                                if name.count > 1 {
//                                    if self.saveDetailsToDb(register: result){
//                                        self.delegate?.sendSchoolInfo(school: name)
//                                        completion(true)
//                                    }else{
//                                        completion(false)
//                                    }
                                    let appuser = AppUserDetails()
//                                    appuser.setUser(id: result.id)
                                    appuser.setMobileVerified(mobile: result.phone_number)
                                    appuser.setPreviousUser(id: result.id)
                                    appuser.setJWT(token: result.token)
                                    self.delegate?.sendSchoolInfo(school: name)
                                    completion(true)
                                }else{
                                    let appuser = AppUserDetails()
//                                    appuser.setUser(id: result.id)
                                    appuser.setPreviousUser(id: result.id)
                                    appuser.setMobileVerified(mobile: result.phone_number)
                                    appuser.setJWT(token: result.token)
                                    completion(true)
                                }

                                
                                
//                                let user = AppUserDetails()
//                                user.setUserUpdate(userId: result.id)
                                
                                
                                
                            }else{
                                let appuser = AppUserDetails()
//                                appuser.setUser(id: result.id)
                                appuser.setMobileVerified(mobile: result.phone_number)
                                appuser.setJWT(token: result.token)
                                completion(true)
                            }
                            
                            
                            
                            
                            
                            
                        }catch{
                            print("unable to parse")
                            completion(false)
                        }
                        
                        
                    }
                    
                    
                case 400...600:
                    print("response error")
                    
                    if let data = response.data {
                        
                        
                        do{
                            
                            let result = try JSONDecoder().decode(OtpVerifyModel.wrongOtp.self, from: data)
                            print(result)
                            
                            otpVerify(result.otp_verify)
                            
                        }catch{
                            print("unable to parse")
                            completion(false)
                        }
                        
                        
                    }
                    
                    completion(false)
                    
                default:
                    print("response error")
                    completion(false)
                }
                
            }
        }
        
        
        
    }
    
    func saveDetailsToDb(register : OtpVerifyModel.OtpVerifyResponse) -> Bool{
        
        let userModel = parseToUserDetailsModel(register : register)
        
        
        let realm = AppSharedDetails.getRealmInstance()
        
            
            do{
                
                try realm.write {
                    realm.add(userModel)
                }
                
            }catch{
                print("unable to save in db")
                return false
            }
        
        print(register.id)
            let appuser = AppUserDetails()
            appuser.setUser(id: register.id)
            appuser.setJWT(token: register.token)
        
        print(AppUserDetails.getUserId())
        
        return true
        
    }
    
    func parseToUserDetailsModel(register : OtpVerifyModel.OtpVerifyResponse) -> UserModel{
        
        let userModel = UserModel()
        
        if let fname = register.first_name,let loc = register.location,let school = register.school_name,let grade = register.grade,let schoolId = register.school,let gradeName = register.grade_name {
            
            
            userModel.first_name = fname
            //        userModel.last_name = register.lastName
            userModel.schoolid = schoolId
            userModel.location = loc
            userModel.school = school
            userModel.gradeid = grade
            userModel.grade = gradeName
//            userModel.userImagePath = image
        }
        
        if let image = register.image{
            userModel.userImagePath = image
        }else{
            if let imgUrl = register.image_url {
                userModel.userImagePath = imgUrl
            }
        }
        
            
        
        
//        let user = AppUserDetails()
//        user.setUserUpdate(userId: register.id)
        
        
        
        return userModel
        
        
        
    }
    
    
    func updateUserWith(location : String,userId : String,schoolId : Int,gradeId : Int,completion : @escaping(Bool) -> Void) {
        
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.users + userId + "/"
        
        let packet = Packets()
        print(url)
        print(userId)
        
        
        NetworkModel.sharedInstance.startNetWorkPatchCall(withURL: url, andParams: packet.createUserUpdatePacketWith(location: location, schoolId: schoolId, gradeId: gradeId)) { (response) in
            print(response.result.value)
            
            if let status = response.response?.statusCode {
                print(status)
                
                switch status {
                    
                case 200...300:
                    print("response success")
                    if let data = response.data {
                        do{
                            let result = try JSONDecoder().decode(OtpVerifyModel.OtpVerifyResponse.self,from: data)
                            
                            if self.changeDetailsInDb(register: result) {
                                
                                completion(true)
                                
                            }else{
                                completion(false)
                            }
                            
                        }catch{
                            print("unable to parse")
                            completion(false)
                            
                            
                        }
                        
                    }
                    
                 
                case 400...600:
                    print("response error")
                    completion(false)
                    
                    
                default:
                    break
                }
                
                
                
            }
            
        }
        
        
    }
    
    func changeDetailsInDb (register : OtpVerifyModel.OtpVerifyResponse) -> Bool {
        
        let userModel = parseToUserDetailsModel(register : register)
        
        
        let realm = AppSharedDetails.getRealmInstance()
        
        
        do{
            
            try realm.write {
                realm.add(userModel)
            }
            
        }catch{
            print("unable to save in db")
            return false
        }
        
        print(register.id)
        let appuser = AppUserDetails()
        appuser.setUser(id: register.id)
        appuser.setJWT(token: register.token)
        
        print(AppUserDetails.getUserId())
        
        return true
        
        
    }
    
    func parseNewUserDetailsModel(register : OtpVerifyModel.OtpVerifyResponse) -> UserModel{
        
        let userModel = UserModel()
        
        if let fname = register.first_name,let loc = register.location,let school = register.school_name,let gradeId = register.grade,let schoolId = register.school,let gradeName = register.grade_name {
            
            
            userModel.first_name = fname
            //        userModel.last_name = register.lastName
            userModel.schoolid = schoolId
            userModel.location = loc
            userModel.school = school
            userModel.gradeid = gradeId
            userModel.grade = gradeName
            //            userModel.userImagePath = image
        }
        
        if let image = register.image{
            userModel.userImagePath = image
        }else{
            if let imgUrl = register.image_url {
                userModel.userImagePath = imgUrl
            }
        }
        
        
        
        
        //        let user = AppUserDetails()
        //        user.setUserUpdate(userId: register.id)
        
        
        
        return userModel
        
    }
    
    
    
}
