//
//  OtpVerifyModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 26/06/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation

class OtpVerifyModel {
    
    struct OtpVerifyResponse : Codable {
        let phone_number : String
        let token : String
        let id : String
        var first_name : String?
        var last_Name : String?
        var location : String?
        var school : Int?
        var grade : Int?
        var image : String?
        var image_url : String?
        var school_name : String?
        var grade_name : String?
        
    }
    
    struct wrongOtp : Codable {
        let otp_verify : Bool
    }
    
}
