//
//  Reachability.swift
//  Fenna
//
//  Created by Rupesh Kadam on 28/06/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import Alamofire

class Reachability {
    
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
    
    
    
}
