//
//  NetworkConstants.swift
//  Fenna
//
//  Created by Rupesh Kadam on 28/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation

class NetworkConstant {
    static var mainURL = "https://fenna-api.analogyplus.com/api/v1/"
    
    struct UrlMethods {
        static let activity = "activity/"
        static let userupdate = "userupdate/"
        static let users = "users/"
        static let userverify = "userverify/"
        static let user = "user/"
        static let activityfilter = "activityfilter/"
        static let useractivity = "useractivity/"
        static let usergradeactivity = "usergradeactivity/"
        static let userfollower = "userfollower/"
        static let activityliked = "activityliked/"
        static let activitylikedcount = "activitylikedcount/"
        static let badge = "badge/"
    }
    
    struct SubUrlMethods {
        static let list = "list/"
        static let user = "user/"
        static let schools = "schools/"
        static let analysis = "analysis2/"
    }
    
    struct AnalysisMethods {
        static let today = "date=today"
        static let week = "date=week"
        static let month = "date=month"
        
    }
    
    struct live {
        static let live = "?status=live"
    }
    
    struct key{
        static let authorization = "Authorization"
        
    }
    
    struct UserParams {
        static let image = "image"
    }
}




