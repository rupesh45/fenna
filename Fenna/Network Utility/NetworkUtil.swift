//
//  NetworkUtil.swift
//  Fenna
//
//  Created by Rupesh Kadam on 28/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import Alamofire

class NetworkModel {
    
    static let sharedInstance = NetworkModel()
    
    
    func getServerToken() -> String {
        return AppUserDetails.getJWTToken()
    }

    
    func startNetworkGetCallWith(URL: String, onCompletion: @escaping (DataResponse<Any>)->Void) -> Void {
        
        Alamofire.request(URL, method: .get, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: onCompletion)
        
    }
    
    func startNetworkPostCallNoHeader(withURL:String,andParams: Parameters,onCompletion: @escaping (DataResponse<Any>)->Void) -> Void {
        

        Alamofire.request(withURL, method: .post, parameters: andParams, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: onCompletion)

        
    }
    
    func startNetworkGetCallWithHeader(URL: String, onCompletion: @escaping (DataResponse<Any>)->Void) -> Void {
        let headers = getServerToken()
        let header: HTTPHeaders = [
            NetworkConstant.key.authorization : "JWT " + headers
        ]
        print("header - \(header)")
        Alamofire.request(URL, method: .get, encoding: JSONEncoding.default, headers: header).responseJSON(completionHandler: onCompletion)
        
    }
    
    func startNetworkPostCall(withURL:String,andParams: Parameters,onCompletion: @escaping (DataResponse<Any>)->Void) -> Void {
        
        let headers = getServerToken()
        let header: HTTPHeaders = [
            NetworkConstant.key.authorization : "JWT " + headers
        ]
        print("header - \(header)")
        Alamofire.request(withURL, method: .post, parameters: andParams, encoding: JSONEncoding.default, headers: header).responseJSON(completionHandler: onCompletion)
        
//        Alamofire.request(withURL, method: .post, parameters: andParams,encoding: JSONEncoding.default).responseJSON(completionHandler: onCompletion)
        
    
        
    }
    
    func startNetWorkPatchCall(withURL: String,andParams: Parameters,onCompletion: @escaping (DataResponse<Any>)->Void) {
        let headers = getServerToken()
        let header: HTTPHeaders = [
            NetworkConstant.key.authorization : "JWT " + headers
        ]
        Alamofire.request(withURL, method: .patch, parameters: andParams,encoding: JSONEncoding.default,headers: header).responseJSON(completionHandler: onCompletion)
    
    }
    
    func startNetworkImagetCallWith(URL: String,method: HTTPMethod,imageParamName: String, image: UIImage?, andParams: Parameters, onCompletion: @escaping (SessionManager.MultipartFormDataEncodingResult)->Void) {
        
        let headers = getServerToken()
        let header: HTTPHeaders = [
            NetworkConstant.key.authorization : "JWT " + headers
        ]
//
//        let URL = try! URLRequest(url: URL, method: method, headers: header)
        
        let URL = try! URLRequest(url: URL, method: method, headers: header)
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            for (key, value) in andParams {
                if let val = value as? String{
                    multipartFormData.append(val.data(using: .utf8)!, withName: key)
                }else if let val = value as? Int{
                    
                    multipartFormData.append("\(val)".data(using: .utf8)!, withName: key)
                    //                    multipartFormData.append(Data(buffer: UnsafeBufferPointer(start: &v, count: 1)) , withName: key)
                }else if let val = value as? Data{
                    
                    multipartFormData.append(val , withName: key)
                }else if let val = value as? Bool{
                    
                    val ? multipartFormData.append("true".data(using: .utf8)!, withName: key) : multipartFormData.append("false".data(using: .utf8)!, withName: key)
                }
                
            }
            
            if let img = image, let imageData = img.jpegData(compressionQuality: 0.5) {
                
                multipartFormData.append(imageData, withName: imageParamName, fileName: "group.jpg", mimeType: "image/jpeg")
            }
            
            
        }, with: URL, encodingCompletion: onCompletion)

    }
    
    func startSingleImagetCallWith(URL: String,method: HTTPMethod,imageParamName: String, image: UIImage?, onCompletion: @escaping (SessionManager.MultipartFormDataEncodingResult)->Void) {
        let headers = getServerToken()
        let header: HTTPHeaders = [
            NetworkConstant.key.authorization : "JWT " + headers
        ]
        
        let URL = try! URLRequest(url: URL, method: method, headers: header)
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
//            for (key, value) in andParams {
//                if let val = value as? String{
//                    multipartFormData.append(val.data(using: .utf8)!, withName: key)
//                }else if let val = value as? Int{
//
//                    multipartFormData.append("\(val)".data(using: .utf8)!, withName: key)
//                    //                    multipartFormData.append(Data(buffer: UnsafeBufferPointer(start: &v, count: 1)) , withName: key)
//                }else if let val = value as? Data{
//
//                    multipartFormData.append(val , withName: key)
//                }else if let val = value as? Bool{
//
//                    val ? multipartFormData.append("true".data(using: .utf8)!, withName: key) : multipartFormData.append("false".data(using: .utf8)!, withName: key)
//                }
//
//            }
            
            if let img = image, let imageData = img.jpegData(compressionQuality: 0.5) {
                
                multipartFormData.append(imageData, withName: imageParamName, fileName: "group.jpg", mimeType: "image/jpeg")
            }
            
            
        }, with: URL, encodingCompletion: onCompletion)
        
    }
    
}



