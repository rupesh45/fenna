//
//  ActivitiesTableViewCell.swift
//  Fenna
//
//  Created by Rupesh Kadam on 22/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit

class ActivitiesTableViewCell: UITableViewCell {

    @IBOutlet weak var ActivityNameLabel: UILabel!
    @IBOutlet weak var ShapeImageView: UIImageView!
    @IBOutlet weak var ActivitiesCountLabel: UILabel!
    @IBOutlet weak var TotalTimeForActivityLabel: UILabel!
    @IBOutlet weak var LikesLabel: UILabel!
    @IBOutlet weak var HeartImageView: UIImageView!
    @IBOutlet weak var GroupImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        ShapeImageView.backgroundColor = UIColor.random()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        self.selectedBackgroundView = backgroundView
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        ActivitiesCountLabel.sizeToFit()
    }
    
}
