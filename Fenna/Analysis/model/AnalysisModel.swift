//
//  AnalysisModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 15/05/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation


class AnalysisModel : Codable {
    
    init() {
        
    }
    
    struct UserAnalysisData : Codable{
        let results : [UserData]
    }
    
    struct UserData : Codable {
        let id : Int
        let user_timespend : Int
        let name : String
        let liked_count : Int
        let user_activity_count : Int
        let activity_url : String
    }
    
    struct UserGradeAnalysisData : Codable{
        let results : [UserGradeData]
    }
    
    struct UserGradeData : Codable {
        let id : Int
        let user_timespend : Int
        let name : String
        let liked_count : Int
        let user_activity_count : Int
    }
    
}
