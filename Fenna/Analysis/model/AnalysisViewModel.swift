//
//  AnalysisModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 22/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireImage

var localTimeZoneName: String { return TimeZone.current.identifier }

class AnalysisViewModel {
    
    static let sharedInstance = AnalysisViewModel()
    
    
    init() {
        
    }
    
    var AllActivities = [AnalysisModel.UserGradeData]()
    var AllPieChartMinutes = [Int]()
    
    var AllUserActivities = [AnalysisModel.UserData]()
    var UserPieChartMinutes = [Int]()
    var allImages : [UIImage] = []
    
    
    
    
    func getUserAnalysisData(userId : String,type : String ,completion : @escaping(Bool) -> Void){
        
        
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.useractivity + NetworkConstant.SubUrlMethods.analysis + "?" + userId + type + "&" + "timezone=\(localTimeZoneName)"
        
        NetworkModel.sharedInstance.startNetworkGetCallWithHeader(URL: url) { (response) in
            if let status = response.response?.statusCode {
                print(status)
                switch status {
                case 200...300:
                    print("response success")
                    if let rdata = response.result.value {
                        print(rdata)
                    }
                    
                    if let data = response.data {
                        do{
                            let results = try JSONDecoder().decode(AnalysisModel.UserAnalysisData.self, from: data)
                            self.AllUserActivities = results.results
                            print(self.AllUserActivities)
                            self.UserPieChartMinutes = []
                            for min in self.AllUserActivities {
//                                if let min  = min.user_timespend {
                                self.UserPieChartMinutes.append(min.user_timespend )
//                                }else{
//                                    self.UserPieChartMinutes.append(0)
//                                }
                                
                            }
                            print(self.UserPieChartMinutes)
                            completion(true)
                            
                        }catch{
                            print("unable to parse")
                            completion(false)
                        }
                        
                        
                        
                    }
                    
//                    completion(true)
                    
                case 400...600:
                    print(response.result.value)
                    print("error")
                    completion(false)
                    
                default:
                    print(response.result.value)
                    print("error")
                    completion(false)
                }
            }
        }
    }
    
    func getAllActivities(type : String ,completion : @escaping(Bool) -> Void){
        
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.usergradeactivity + NetworkConstant.SubUrlMethods.analysis + "?" + type + "&" + "timezone=\(localTimeZoneName)"
        
        print(url)
        
        NetworkModel.sharedInstance.startNetworkGetCallWithHeader(URL: url) { (response) in
           if let status = response.response?.statusCode {
                print(status)
                switch status {
                case 200...300:
                    print(response.result.value)
                    if let data = response.data{
                        do{
                            let results = try JSONDecoder().decode(AnalysisModel.UserGradeAnalysisData.self, from: data)
                            self.AllActivities = results.results
                            self.AllPieChartMinutes = []
                            for min in self.AllActivities{
                                
                                self.AllPieChartMinutes.append(min.user_timespend)
                                
                            }
                            
                            completion(true)
                        }catch{
                            print("unable to parse")
                            completion(false)
                        }
                        
                    }
                   
                case 400...600:
                    print("response error")
                    print(response.result.value)
                    completion(false)
                default:
                    print("default error")
                    completion(false)
                }
            }
        }
        
    }
    
    func getMinutes(index : Int,type : UserProfileModel.UserActivitiesTypes) -> Int {
        
        switch type {
        case .My:
            return self.UserPieChartMinutes[index]
            
        case .Group:
            return self.AllPieChartMinutes[index]
            
        case .FollowedUser:
            return self.UserPieChartMinutes[index]
            
        }
        
        
    }
    
    func getAllActivitiesCount(type : UserProfileModel.UserActivitiesTypes) -> Int {

        switch type {
        case .My:
            return AllUserActivities.count
            
        case .Group:
            return self.AllActivities.count
            
        case .FollowedUser:
            return AllUserActivities.count
            
        }
        
    }
    
    func getAllActivitiesName(index : Int,type : UserProfileModel.UserActivitiesTypes) -> String {

        switch type {
        case .My:
            return AllUserActivities[index].name
            
        case .Group:
            return AllActivities[index].name
            
        case .FollowedUser:
            
            return AllUserActivities[index].name
            
        }
        
        
        
    }
    
    func getTotalTimeSpent(index:Int,type : UserProfileModel.UserActivitiesTypes) -> String {

        switch type {
        case .My:
            return String(AllUserActivities[index].user_timespend)
            
        case .Group:
            return String(AllActivities[index].user_timespend)
            
        case .FollowedUser:
            return String(AllUserActivities[index].user_timespend)
            
        }
        
    }
    
    func getActivitiesCount(index: Int,type : UserProfileModel.UserActivitiesTypes) -> String {

        switch type {
        case .My:
            return String(AllUserActivities[index].user_activity_count)
            
        case .Group:
           return String(AllActivities[index].user_activity_count)
            
        case .FollowedUser:
            return String(AllUserActivities[index].user_activity_count)
            
        }

        
    }
    
    func getLikesCount(index : Int,type : UserProfileModel.UserActivitiesTypes) -> String {

        switch type {
        case .My:
            return String(AllUserActivities[index].liked_count)
            
        case .Group:
            return String(AllActivities[index].liked_count)
            
        case .FollowedUser:
            return String(AllUserActivities[index].liked_count)
            
        }
    }
    
    func activityShapeImage(index: Int) -> UIImage {
        var images : [UIImage?] = [UIImage(named: "shape1"),UIImage(named: "shape2"),UIImage(named: "shape3"),UIImage(named: "shape4"),UIImage(named: "shape5")]
        
        return images[index]!
        
    }
    

    
    private func GetActivityImage(withUrl :URL, completion : @escaping(UIImage) -> Void ){
        var activityImage = UIImage()
        Alamofire.request(withUrl).responseImage { (response) in
            if let img = response.result.value {
                activityImage = img
                completion(activityImage)
            }
        }
        
    }
    
    
    //search model purpose
    func getAllActName(completion : @escaping([String]) -> Void) {
        
        
        getUserAnalysisData(userId: "", type: NetworkConstant.AnalysisMethods.month) { (bool) in
            if bool {
                var nameArray : [String] = []
                
                for data in self.AllUserActivities {
                    nameArray.append(data.name)
                }
                
                completion(nameArray)
                
            }else{
//                return ["",""]
                print("error")
            }
           
        }
        
    }
    
    func sendUserData() -> [AnalysisModel.UserData] {
        return AllUserActivities
    }
    
//    func getAllTagsCount() -> Int {
//
//        return AllUserActivities.count
//    }
//
//    func getTagName(index : Int) -> String {
//        return AllUserActivities[index].name
//    }
//
//    func getTagId(index : Int) -> Int {
//        return AllUserActivities[index].id
//    }
//
//    func activityImage(index : Int,completion : @escaping (UIImage) -> Void){
//
//
//        let urlString = AllUserActivities[index].activity_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
//        if  let url = URL(string: urlString){
//            GetActivityImage(withUrl: url) { (image) in
//                completion(image)
//            }
//        }
//
//
//    }
    
//    func getAllImages(completion : @escaping (Bool) -> Void){
//        //        var tempImageArray:[UIImage] = []
//         allImages = []
//        for activity in self.AllUserActivities {
//            GetActivityImage(withUrl: URL(string: activity.activity_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!) { (img) in
//                self.allImages.append(img)
//                print(img)
//            }
//        }
//
//        //        completion(true)
//
//    }
    
    
    
    
}


