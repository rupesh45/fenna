//
//  AnalysisViewController.swift
//  Fenna
//
//  Created by Rupesh Kadam on 18/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit
import Charts
import NVActivityIndicatorView
import RealmSwift

class AnalysisViewController: BackGroundViewController {

    @IBOutlet weak var TodayButton: UIButton!
    @IBOutlet weak var WeekButton: UIButton!
    @IBOutlet weak var MonthButton: UIButton!
    @IBOutlet weak var pieChart: PieChartView!
    @IBOutlet weak var ActivityTableView: UITableView!
    @IBOutlet weak var PreviousButton: UIButton!
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    @IBOutlet weak var TitleLabel: UILabel!
    
    @IBOutlet weak var BackView: UIView!
    @IBOutlet weak var nextButton: UIButton!
    var analysisModel : AnalysisViewModel!
    var activityModel : MainActivityModel!
    var entries : [PieChartDataEntry] = []
    
    var refreshControl = UIRefreshControl()
    
    var datetoday = NetworkConstant.AnalysisMethods.today
    var dateweek = NetworkConstant.AnalysisMethods.week
    var datemonth = NetworkConstant.AnalysisMethods.month
    var userId = ""
    
    
    var realm = AppSharedDetails.getRealmInstance()
    var followTable : Results<FollowerTable>!
    
    
    var dayType : UserProfileModel.Day = .Today {
        didSet {
            print("new dattype val \(dayType)")
            getAnalysisData(day: dayType, type: actType)
            
            switch dayType {
            case .Today:
                print("t")
                highlightTodayButton(buttonToHighlight: TodayButton, button2: WeekButton, button3: MonthButton)
            case .Week:
                print("w")
                highlightTodayButton(buttonToHighlight: WeekButton, button2: TodayButton, button3: MonthButton)
            case .Month:
                print("m")
                highlightTodayButton(buttonToHighlight: MonthButton, button2: TodayButton, button3: WeekButton)
                
            }
        }
    }
    
    var actType : UserProfileModel.UserActivitiesTypes = .My {
        didSet {
            
            print("new acttype val \(actType)")
            getAnalysisData(day: dayType, type: actType)
            TitleLabel.text = actType.TitleForActTypes()
            
            switch actType {
            case .My:
                print("m")
                
            case .Group:
                print("g")
                
            case .FollowedUser:
                print("f")
                TitleLabel.text = followTable[0].username
                
            }
            
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityModel = MainActivityModel.sharedInstance
        
        activityIndicator.setupActivityIndicator()
        
        PreviousButton.alpha = 0.3
         
        followTable = realm.objects(FollowerTable.self)
        print(followTable)
        
        getAnalysisData(day: dayType, type: actType)
        
        ActivityTableView.delegate = self
        ActivityTableView.dataSource = self
        ActivityTableView.registerNib(nibName: "ActivitiesTableViewCell", identifier: "cell")
        // Do any additional setup after loading the view.
        
        pieChart.setupPieChart()
        BackView.layer.cornerRadius = 10
        BackView.layer.masksToBounds = true
        BackView.backgroundColor = UIColor(hexString: "#2E58E6")
        
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        ActivityTableView.refreshControl = refreshControl
        
        
        TitleLabel.text = UserProfileModel.UserActivitiesTypes.My.TitleForActTypes()
        
        NotificationCenter.default.addObserver(self, selector: #selector(UserUnfollowed), name: .refreshAnalytics, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(UserUnfollowedRoleModel), name: .hideFollowAndAnalytics, object: nil)
    }
    
    

    
    @objc func onRefresh(refreshControl: UIRefreshControl) {
        
        getAnalysisData(day: dayType, type: actType)
        
    }
    
    @objc func UserUnfollowed(_ : Notification) {
        
        actType = .Group
        nextButton.isUserInteractionEnabled = true
        nextButton.alpha = 1
        
    }
    
    @objc func UserUnfollowedRoleModel(_ : Notification) {
        
        actType = .Group
        nextButton.isUserInteractionEnabled = true
        nextButton.alpha = 1
        
    }
    
    private func highlightTodayButton(buttonToHighlight button1 : UIButton,button2 : UIButton,button3 : UIButton) {
        
        if button1.alpha == 0.5 {
            button1.alpha = 1
            button2.alpha = 0.5
            button3.alpha = 0.5
        }
        
        
    }
    
    
    private func getAnalysisData(day : UserProfileModel.Day,type : UserProfileModel.UserActivitiesTypes){
        activityIndicator.startAnimating()
        
        
        switch type {
        case .My:
            print("my")
            
            analysisModel.getUserAnalysisData(userId: "", type: day.rawValue) { (complete) in
                if complete {
                    
                    print("success")
                    self.entries.removeAll()
                    self.pieChart.notifyDataSetChanged()
                    self.pieChart.data = nil
                    
                    for (index,min) in self.analysisModel.UserPieChartMinutes.enumerated() {
                        print(index,min)
                        
                        self.pieChartUpdate(index: index)
                    }
                    
                    self.refreshControl.endRefreshing()
                    self.ActivityTableView.reloadData()
                    self.activityIndicator.stopAnimating()
                }else{
                    self.refreshControl.endRefreshing()
                    print("response error")
                    self.activityIndicator.stopAnimating()
                }
            }
            
        case .Group:
            print("group")
            
            analysisModel.getAllActivities(type: day.rawValue) { (complete) in
                if complete {
                    print("success")
                    self.entries = []
                    self.pieChart.notifyDataSetChanged()
                    self.pieChart.data = nil
                    
                    for (index,min) in self.analysisModel.AllPieChartMinutes.enumerated() {
                        print(index,min)
                        
                        self.pieChartUpdate(index: index)
                    }
                    
                    self.refreshControl.endRefreshing()
                    //                self.pieChartUpdate()
                    self.ActivityTableView.reloadData()
                    self.activityIndicator.stopAnimating()
                }else{
                    self.refreshControl.endRefreshing()
                    print("response error")
                    self.activityIndicator.stopAnimating()
                }

            }

        case .FollowedUser:
            print("followed user")
            
                let userId = followTable[0].user_id
                analysisModel.getUserAnalysisData(userId: "user_id=" + userId + "&", type: day.rawValue) { (complete) in
                    if complete{
                        
                        print("success")
                        self.entries.removeAll()
                        self.pieChart.notifyDataSetChanged()
                        self.pieChart.data = nil
                        
                        for (index,min) in self.analysisModel.UserPieChartMinutes.enumerated() {
                            print(index,min)
                            
                            self.pieChartUpdate(index: index)
                        }
                        
                        self.refreshControl.endRefreshing()
                        self.ActivityTableView.reloadData()
                        self.activityIndicator.stopAnimating()
                    }else{
                        self.refreshControl.endRefreshing()
                        print("response error")
                        self.activityIndicator.stopAnimating()
                    }
                }
            
            
        }
        
        
    }
    

    private func pieChartUpdate (index : Int) {
        
        
        let entry : PieChartDataEntry = PieChartDataEntry(value: Double(analysisModel.getMinutes(index: index, type: actType)), label: "")
        
        entries.append(entry)
        print("entries \(entries)")
        let dataSet = PieChartDataSet(entries: entries, label: "")
        print(dataSet)
        
        
        
        dataSet.valueTextColor = UIColor.clear
        dataSet.valueLineWidth = 10
        let data = PieChartData(dataSet: dataSet)
        pieChart.data = data
        
        //All other additions to this function will go here
        dataSet.colors = [UIColor(hexString: "#8BE1FF"),UIColor(hexString: "#FF9A9E"),UIColor(hexString: "#00C51B"),UIColor(hexString: "#F98712"),UIColor(hexString: "#8859FE")]
        
        pieChart.holeRadiusPercent = 0
        pieChart.drawHoleEnabled = false
//        pieChart.backgroundColor = UIColor(hexString: "2E58E6")
        pieChart.backgroundColor = UIColor.clear
        pieChart.chartDescription?.textColor = UIColor.clear
        pieChart.legend.textColor = UIColor.white
        
        //This must stay at end of function
        pieChart.notifyDataSetChanged()
        
    }
    
    
    @IBAction func OnTodayButtonTapped(_ sender: UIButton) {

        dayType = .Today
        
        
    }
    
    @IBAction func OnWeekButtonTapped(_ sender: UIButton) {

        dayType = .Week
        
    }
    
    @IBAction func OnMonthButtonTapped(_ sender: UIButton) {

        dayType = .Month
        
        
    }
    
    @IBAction func OnNextButtonTapped(_ sender: UIButton) {
        
        
        switch actType {
        case .My:
            
            actType = .Group
            PreviousButton.alpha = 1
            PreviousButton.isUserInteractionEnabled = true
            
        case .Group:
            
            if followTable.count > 0 {
                
                actType = .FollowedUser
                nextButton.alpha = 0.3
                nextButton.isUserInteractionEnabled = false
                
            }else{
                self.activityIndicator.stopAnimating()
//                errorAlert(title: "Please Follow", message: "you are not following anyone!", vc: self)
                
            }
            
        case .FollowedUser:
            
            print("foll")
        }
        
        
        
    }
    
    @IBAction func OnPreviousButtonTapped(_ sender: UIButton) {
        
        switch actType {
        case .My:

            print("my")
            
        case .Group:
            
            actType = .My
            PreviousButton.alpha = 0.3
            PreviousButton.isUserInteractionEnabled = false
            
        case .FollowedUser:
            
            actType = .Group
            nextButton.isUserInteractionEnabled = true
            nextButton.alpha = 1
            
        }
        
        
    }
    
}

extension AnalysisViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return analysisModel.getAllActivitiesCount(type: actType)
//        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ActivitiesTableViewCell

        
        cell.ActivityNameLabel.text = analysisModel.getAllActivitiesName(index: indexPath.row, type: actType)
        cell.ShapeImageView.image = analysisModel.activityShapeImage(index: indexPath.row)
        cell.TotalTimeForActivityLabel.text = analysisModel.getTotalTimeSpent(index: indexPath.row, type: actType) + "\n" + "min"
        cell.LikesLabel.text = analysisModel.getLikesCount(index: indexPath.row, type: actType)
        
        switch actType {
        case .My:
            print("my")
            cell.GroupImageView.isHidden = true
            if Int(analysisModel.getActivitiesCount(index: indexPath.row, type: actType))! <= 1{
                cell.ActivitiesCountLabel.text = analysisModel.getActivitiesCount(index: indexPath.row, type: actType) + " activitiy"
            }else{
                cell.ActivitiesCountLabel.text = analysisModel.getActivitiesCount(index: indexPath.row, type: actType) + " activities"
            }
            
            cell.LikesLabel.isHidden = false
            cell.HeartImageView.isHidden =  false
            
        case .Group:
            print("grp")
            
            cell.GroupImageView.isHidden = false
            cell.ActivitiesCountLabel.text = analysisModel.getActivitiesCount(index: indexPath.row, type: actType)
            
            
            cell.LikesLabel.isHidden = true
            cell.HeartImageView.isHidden =  true

            
        case .FollowedUser:
            print("foll")
            
            cell.GroupImageView.isHidden = true
            if Int(analysisModel.getActivitiesCount(index: indexPath.row, type: actType))! <= 1{
                cell.ActivitiesCountLabel.text = analysisModel.getActivitiesCount(index: indexPath.row, type: actType) + " activitiy"
            }else{
                cell.ActivitiesCountLabel.text = analysisModel.getActivitiesCount(index: indexPath.row, type: actType) + " activities"
            }
            cell.LikesLabel.isHidden = true
            cell.HeartImageView.isHidden =  true
            
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
}


