//
//  TagsCollectionViewCell.swift
//  Fenna
//
//  Created by Rupesh Kadam on 10/06/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit

class TagsCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var TagNameLabel: UILabel!
    @IBOutlet weak var CrossButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        self.backgroundColor = UIColor.white
        TagNameLabel.textColor = .white
        self.layer.cornerRadius = 17
        self.layer.masksToBounds = true
        
    }
    
}
