//
//  SearchActivityModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 14/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage
import Alamofire

class SearchActivityModel {
    
    let activityModel = MainActivityModel.sharedInstance
    
    var SearchResults = [SearchModel.SearchResults]()
    
    var tags : [SearchModel.Tags] = []
    
    var UserActivityData : [AnalysisModel.UserData] = []
   
    var analysisModel = AnalysisViewModel.sharedInstance
    
    init() {
        
    }
    
    
    
    func searchActivity(searchWord : String , completion : @escaping (Bool) -> Void){
        
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.activity + NetworkConstant.SubUrlMethods.list + "?order_by=" + searchWord
        
        NetworkModel.sharedInstance.startNetworkGetCallWithHeader(URL: url) { (response) in
            if let status = response.response?.statusCode {
                print(status)
                
                switch status {
                case 200...300:
                    print(response.result.value)
                    
                    if let data = response.data {
                        do{
                            let results = try JSONDecoder().decode(SearchModel.SearchData.self, from: data)
                            
                            self.SearchResults =  results.results
                            completion(true)
                        }catch{
                            print("unable to pasre")
                            completion(false)
                        }
                    }
                  
                case 400...600:
                    print(response.result.value)
                    print("response error")
                    completion(false)
                    
                default:
                    print(response.result.value)
                    print("response error default")
                    completion(false)
                }
            }
        }
        
    }
    
    func getSearchResultsName(index : Int) -> String {
        return SearchResults[index].name
    }
    
    func getSearchResultId(index : Int) -> Int {
        return SearchResults[index].id
    }
    
    func getsearchCount() -> Int {
        return SearchResults.count
    }
    
    func getSearchImage(index : Int,completion : @escaping (UIImage) -> Void){
        
        if let urlString = SearchResults[index].activity_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed){
            if let url = URL(string: urlString) {
                GetActivityImage(withUrl: url) { (img) in
                    completion(img)
                }
            }
        }
        
        
        
    }
    
    private func GetActivityImage(withUrl :URL, completion : @escaping(UIImage) -> Void ){
        var activityImage = UIImage()
        Alamofire.request(withUrl).responseImage { (response) in
            if let img = response.result.value {
                activityImage = img
                completion(activityImage)
            }
        }
        
    }
    
    func getTagsCount() -> Int {
        return UserActivityData.count
    }
    
    
    
    func getActivityNamesForTags(completion : @escaping([String],Bool) -> Void ) {
        

        analysisModel.getAllActName { (stringArray) in
            completion(stringArray, true)
        }

        
    }
    
    func activityImage(index : Int,completion : @escaping (UIImage) -> Void){
        
        
        let urlString = UserActivityData[index].activity_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        if  let url = URL(string: urlString){
            GetActivityImage(withUrl: url) { (image) in
                completion(image)
            }
        }
        
        
    }
    
    func getTagsName(index : Int) -> String {
        return UserActivityData[index].name
    }
    
    func getActivityImage(index : Int,completion : @escaping(UIImage) -> Void) {

        activityImage(index: index) { (img) in
            completion(img)
        }
        
    }
    
    func getTagId(index : Int) -> Int {
        return UserActivityData[index].id
    }
    
    
    func setData(){
        self.UserActivityData = analysisModel.sendUserData()
    }
    
    
    //design here
    
    func setupSearchBar(searchBar : UISearchBar) {

        searchBar.barTintColor = UIColor.blue
        searchBar.backgroundImage = UIImage()
        
        
        searchBar.setPlaceholderTextColorTo(color: UIColor.white)
        searchBar.setMagnifyingGlassColorTo(color: UIColor.white)
        
    }
}
