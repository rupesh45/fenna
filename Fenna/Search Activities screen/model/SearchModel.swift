//
//  SearchModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 06/06/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation


class SearchModel {
    
    
    struct SearchData : Codable {
        var results = [SearchResults]()
    }
    
    struct SearchResults : Codable {
        var id : Int
        var name : String
        var activity_url : String
    }
    
    struct Tags {
        var id : Int
        var name : String
        var activity_url : String
    }
    
    
}
