//
//  SearchActivitiesViewController.swift
//  Fenna
//
//  Created by Rupesh Kadam on 14/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit

protocol SelctedActivityFromSearch : class {
    func onSelectedActivity(actId : Int,activity : String,image : UIImage) -> Bool
}

class SearchActivitiesViewController: BackGroundViewController {

    @IBOutlet weak var SearchBar: UISearchBar!
    @IBOutlet weak var ActivitiesCollectionView: UICollectionView!
    @IBOutlet weak var TagsCollectionView: UICollectionView!
    
    var activityModel : MainActivityModel!
    var searchActivityModel : SearchActivityModel!

    
    var searched = false
 
    weak var delegate : SelctedActivityFromSearch?
   

    
    override func viewDidLoad() {
        super.viewDidLoad()

        ActivitiesCollectionView.delegate = self
        ActivitiesCollectionView.dataSource = self
    
        
        TagsCollectionView.delegate = self
        TagsCollectionView.dataSource = self

        
        SearchBar.delegate = self
//        searchActivityModel.setupSearchBar(searchBar: SearchBar)
        
        searchActivityModel.getActivityNamesForTags { (_, bool) in
            if bool {
                self.searchActivityModel.setData()
                self.TagsCollectionView.reloadData()
            }else{
                print("response err tags")
            }
        }

        
        
        ActivitiesCollectionView.registerNib(nibName: "ActivityCollectionViewCell", identifier: "cell")
        

    }
    
    override func viewDidLayoutSubviews() {
        
        print(self.view.frame.width)
        print(self.view.frame.height)
        
        activityModel.cellLayout(view: self.view, collectionView: ActivitiesCollectionView)
        searchActivityModel.setupSearchBar(searchBar: SearchBar)
        
    }
    
    @objc func DeleteTag(_ sender : UIButton){
        
        let indexPath = IndexPath(item: sender.tag, section: 0)
        searchActivityModel.UserActivityData.remove(at: indexPath.row)
        TagsCollectionView.performBatchUpdates({
            TagsCollectionView.deleteItems(at: [indexPath])
        }) { (bool) in
            self.TagsCollectionView.reloadItems(at: self.TagsCollectionView.indexPathsForVisibleItems)
        }
        
    }

}


extension SearchActivitiesViewController: UISearchBarDelegate {
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        let trimmedString = searchBar.text?.components(separatedBy: .whitespaces).joined().addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let trimmedString = searchBar.text?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        print(trimmedString)
        
        searchActivityModel.searchActivity(searchWord: trimmedString!) { (bool) in
            if bool {
                print("search succes")
                self.searched = true
                self.ActivitiesCollectionView.reloadData()
                searchBar.resignFirstResponder()
            }else{
                print("response error")
            }
        }
    }
    
}


extension SearchActivitiesViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == TagsCollectionView {
            return searchActivityModel.getTagsCount()
        }else{
            if !searched {
                return activityModel.getActivityCount()
            }else{
                return searchActivityModel.getsearchCount()
            }
        }
        
       
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == TagsCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TagsCollectionViewCell
            
            cell.CrossButton.tag = indexPath.row
            cell.TagNameLabel.text = searchActivityModel.getTagsName(index: indexPath.row)
            cell.CrossButton.addTarget(self, action: #selector(DeleteTag(_:)), for: .touchUpInside)
            return cell
            
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ActivityCollectionViewCell
            
            if !searched {
                
                cell.ActivityNameLabel.text = activityModel.getActivityName_MainActModel(index: indexPath.row)
                
                
                let url = activityModel.mainActivityData[indexPath.row].activity_url
                cell.ActivityImageView.loadImageFromurl(urlString: url)
                
                
                
                
            }else{
                cell.ActivityNameLabel.text =  searchActivityModel.getSearchResultsName(index: indexPath.row)
                let url = searchActivityModel.SearchResults[indexPath.row].activity_url
                cell.ActivityImageView.loadImageFromurl(urlString: url)
            }
            
            return cell
        }
        
        
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == TagsCollectionView {
            
            let actId = searchActivityModel.getTagId(index : indexPath.row)
            let actname = searchActivityModel.getTagsName(index: indexPath.row)
            
            searchActivityModel.getActivityImage(index: indexPath.row) { (img) in
                self.delegate?.onSelectedActivity(actId: actId, activity: actname , image: img)
            }
            self.navigationController?.popViewController(animated: true)
            
        }else{
            if !searched {
                
                let actId = activityModel.getActivityId_MainActModel(index: indexPath.row)
                let actname = activityModel.getActivityName_MainActModel(index: indexPath.row)
                
                activityModel.getActivityImage(index: indexPath.row) { (img) in
                    self.delegate?.onSelectedActivity(actId: actId, activity: actname, image: img)
                    
                }
                
                
                
                
                
                self.navigationController?.popViewController(animated: true)
            }else{
                
                let actId = searchActivityModel.getSearchResultId(index: indexPath.row)
                let actname = searchActivityModel.getSearchResultsName(index: indexPath.row)
                
                searchActivityModel.getSearchImage(index: indexPath.row) { (img) in
                    self.delegate?.onSelectedActivity(actId: actId, activity: actname, image: img)
                }
                
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let lastItem = activityModel.mainActivityData.count - 1
        if indexPath.row == lastItem {
            print("request more data")
            loadMoreActs()
        }
        
    }
    
    func loadMoreActs() {
        activityModel.loadMoreActivities { (bool) in
            if bool {
                self.ActivitiesCollectionView.reloadData()
            }else{
                print("response error loading more activities.")
            }
        }
    }

    
    
}
