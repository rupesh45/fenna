//
//  CircularProgressView.swift
//  Fenna
//
//  Created by Rupesh Kadam on 02/04/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit

class CircularProgressView: UIView {

    var progressLayer = CAShapeLayer()
    var trackLayer = CAShapeLayer()
    var gradientLayer = CAGradientLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        createCircularPath(rect: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createCircularPath(rect: frame)
    }
    
    override func draw(_ rect: CGRect) {
       
        createCircularPath(rect: rect)
    }
    

    
    var progressColor = UIColor.white {
        didSet {
            progressLayer.strokeColor = progressColor.cgColor
        }
    }
    
    var trackColor = UIColor.blue {
        didSet {
            trackLayer.strokeColor = trackColor.cgColor
        }
    }
    
    func createCircularPath(rect : CGRect){
        
         let linewidth = 0.2 * min(rect.width,rect.height)
        let radius = (min(rect.width,rect.height) - linewidth) / 2
        self.backgroundColor = .clear
        self.layer.cornerRadius = self.frame.width/2
        let circularPath = UIBezierPath(arcCenter: CGPoint(x: rect.width/2, y: rect.height/2), radius: radius, startAngle: CGFloat(-0.5 * .pi), endAngle: CGFloat(1.5 * .pi), clockwise: true)
        //trackLayer
        trackLayer.path = circularPath.cgPath
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.strokeColor = trackColor.cgColor
        trackLayer.frame = rect
        trackLayer.lineWidth = 20
        trackLayer.strokeEnd = 1.0
        layer.addSublayer(trackLayer)
        
        //progressLayer
        progressLayer.path = circularPath.cgPath
        progressLayer.fillColor = UIColor.clear.cgColor
        progressLayer.strokeColor = progressColor.cgColor
        progressLayer.lineCap = .round
        progressLayer.frame = rect
        progressLayer.lineWidth = 25
        progressLayer.strokeEnd = 0
        
        //gradientlayer
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)

        gradientLayer.frame = rect
        gradientLayer.colors = [UIColor.yellow.cgColor,UIColor.red.cgColor]
        gradientLayer.mask = progressLayer
//        layer.addSublayer(progressLayer)
        layer.addSublayer(gradientLayer)
    
    }
    
    func startProgressWith(duration : TimeInterval,fromValue : Float,toValue : Float) {
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.duration = duration
        animation.fromValue = fromValue
        animation.toValue = toValue
        animation.timingFunction = CAMediaTimingFunction(name: .linear)
        animation.isRemovedOnCompletion = false
        progressLayer.strokeEnd = CGFloat(toValue)
        progressLayer.add(animation, forKey: "progress")
        
    }
}
