//
//  LiveSessionModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 12/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import UIKit
import UICircularProgressRing
import RealmSwift
import Alamofire

class LiveSessionModel {
    
     
    var timerLabelText = ""
    var myActivity = ""
    
    var circularProgress = CircularProgressView()
    
    var completionHandler : ((Bool) -> ())? = nil
    
    init() {
        
    }

    
    func endActivityPatchCallWith(totalMinutes : Int,liveActId : Int, completion :@escaping (Bool) -> Void){
        
        completionHandler = completion
        
        let packetCreator = Packets()
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.activity + NetworkConstant.SubUrlMethods.user + String(liveActId) + "/"
        NetworkModel.sharedInstance.startNetWorkPatchCall(withURL: url, andParams: packetCreator.createEndActivityPacketWith(pre_duration: totalMinutes), onCompletion: onCompletion)
        
    }
    
    func onCompletion(responseData : DataResponse<Any>) {
        
        if let status = responseData.response?.statusCode {
            print(status)
            switch status {
            case 200...300:
                print(responseData.result.value)
                if let callback = self.completionHandler {
                    callback(true)
                }
                
            case 400...600:
                print(responseData.result.value)
                if let callback = self.completionHandler {
                    callback(false)
                }
            default:
                if let callback = self.completionHandler {
                    callback(false)
                }
            }
            
        }
    }
    
    func getTotalLikeCount(activityId : Int, completion : @escaping(Bool,Int) -> Void){
        
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.activitylikedcount
        
        let parameters : Parameters = ["activity" : activityId]
        
        NetworkModel.sharedInstance.startNetworkPostCall(withURL: url, andParams: parameters) { (response) in
            if let status = response.response?.statusCode {
                print(status)
                switch status {
                case 200...300:
                    print("response success")
                    print(response.result.value)
                    
                    if let data = response.result.value as? [String : Any] {
                        let count = data["count"] as! Int
                        completion(true, count)
                    }
                    
                    
                case 400...600:
                    print("response error")
                    print(response.result.value)
                    completion(false, 0)
                default:
                    print("response err default")
                    completion(false, 0)
                }
                
            }
        }
        
        
        
        
        
    }
    
    
    //design here
    
    func setupEndButton(button : UIButton){
        
        button.layer.cornerRadius = button.frame.height / 2
        button.layer.masksToBounds = true
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 1
        
        
    }
    
    
    func addProgressRing(view : UIView){
        circularProgress = CircularProgressView(frame: CGRect(x: 0, y: 0, width: 320, height: 320))
        circularProgress.trackColor = UIColor(hexString: "#5506F1")
        //        circularProgress.progressColor = UIColor(hexString: "#00AEFF")
        circularProgress.center = view.center
        circularProgress.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        view.addSubview(circularProgress)
    }
    

    
}
