//
//  InstaStoryView.swift
//  Fenna
//
//  Created by Rupesh Kadam on 17/07/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit

class InstaStoryView: UIView {
    
    
    @IBOutlet weak var UserNameLabel: UILabel!
    @IBOutlet weak var MinutesLabel: UILabel!
    @IBOutlet weak var ActivityLabel: UILabel!
    @IBOutlet weak var FinnaLogo: UIImageView!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "InstaStoryView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        self.setThemeBackground()
//        self.FinnaLogo.roundCorners()
    }

}
