//
//  LiveSessionViewController.swift
//  Fenna
//
//  Created by Rupesh Kadam on 12/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import UIKit
import UICircularProgressRing
import RealmSwift
import NVActivityIndicatorView



class LiveSessionViewController: UIViewController,UIPopoverPresentationControllerDelegate {


    @IBOutlet weak var EndButton: UIButton!
    @IBOutlet weak var ExtendButton: UIButton!
    @IBOutlet weak var minutesLabel: UILabel!
    @IBOutlet weak var ActivityNameLabel: UILabel!
    
    @IBOutlet weak var minutesLeftLabel: UILabel!
    @IBOutlet weak var progressView: CircularProgressView!
    
    @IBOutlet weak var ActivityIndicatorView: UIView!
    @IBOutlet weak var ActivityIndicator: NVActivityIndicatorView!
    
    var liveSessionModel : LiveSessionModel!
    var schoolSelectionModel : SchoolSelectionModel!
    var activityModel : MainActivityModel!

    var minutes = Float()
    var timer = Timer()

    var end : Date!
    
    var backgroundTask : UIBackgroundTaskIdentifier! = .invalid
    
    
    var activity : Results<Activity>!
    var userAct : Results<userActivityTable>!
    
    var activityId = 0
    
    var fromvalue = Float()
    
    let realm = AppSharedDetails.getRealmInstance()
    
    var likesCount = 0
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        calToggleValue()

        ActivityIndicator.setupActivityIndicator()

        activity = realm.objects(Activity.self)
        userAct = realm.objects(userActivityTable.self)
        
        schoolSelectionModel.SetupDesignBack(view: self.view)
        liveSessionModel.addProgressRing(view: self.view)
        liveSessionModel.setupEndButton(button: EndButton)

        ActivityNameLabel.text = liveSessionModel.myActivity
        
        print(returnRemainingSec(finishDate: self.end))
        
        
        BeginTimer()
        liveSessionModel.circularProgress.startProgressWith(duration: TimeInterval(returnRemainingSec(finishDate: self.end)), fromValue: fromvalue, toValue: 1)
        
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        

    }
    


    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.hidesBackButton = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.hidesBackButton = false
        self.tabBarController?.tabBar.isHidden = false
//
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    @objc func willEnterForeground(_ notification : NSNotification){
        
        print("foreground me")
        
        calToggleValue()
        BeginTimer()
        liveSessionModel.circularProgress.startProgressWith(duration: TimeInterval(returnRemainingSec(finishDate: self.end)), fromValue: fromvalue, toValue: 1)
        
        
        
    }
    
    @objc func willEnterBackground(_ notification : NSNotification){
        
        print("timer ended")
        endTimer()
        
    }
    
    
//
//
//    func roundToFifteen(value: Int) -> Int{
//        let fractionNum = Double(value) / 15.0
//        let roundedNum = Int(ceil(fractionNum))
//        return roundedNum * 15
//    }
    
    //move this to livesession model
    func calToggleValue(){
        
        var toggle = Float()
        
        var totalToggle = Float()
        
        totalToggle = 1 / (minutes * 60)
        toggle = Float(1) / Float(returnRemainingSec(finishDate: self.end))
        fromvalue = Float()
        
        fromvalue = totalToggle / toggle
        
        if fromvalue == 1 {
            fromvalue = 0
        }else{
            fromvalue = 1 - (totalToggle / toggle)
        }
        
    }

    //move this to livesession model
    func timeRemainingString(finishDate date:Date) -> String {
        let secondsFromNowToFinish = date.timeIntervalSinceNow
        let hours = Int(secondsFromNowToFinish / 3600)
        let minutes = Int((secondsFromNowToFinish - Double(hours) * 3600) / 60)
        let seconds = Int(secondsFromNowToFinish - Double(hours) * 3600 - Double(minutes) * 60 + 0.5)
        
        if hours == 1 {
            self.minutesLeftLabel.text = "hour left"
            return String(format: "%02d:%02d:%02d",hours, minutes, seconds)
        }
        
        if minutes == 0 {
            self.minutesLeftLabel.text = "seconds left."
            return String(seconds)
        }else{
//        print(minutes)
//        return String(minutes + 1)
            return String(format: "%02d:%02d", minutes, seconds)
        }
        
        
        
    }
    
    //move this to livesession model
    func returnRemainingSec(finishDate date:Date) -> Int {
        let secondsFromNowToFinish = date.timeIntervalSinceNow
        return Int(secondsFromNowToFinish)
    }
    
    func removeLiveActivity(){
        let obj = self.userAct.filter("is_active = \(true)")
        try! realm.write {
            obj[0].is_active = false
        }
    }
    
    
    
    @IBAction func onEndButtonTapped(_ sender: UIButton) {
        
            //add total time to selected activity here
            if Date() > self.end {
//
        }else {
            let alert = UIAlertController(title: "Are you sure?", message: "You’re going to end the current activity before the time set.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (alert) in
                
            }))
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert) in
//                self.ActivityIndicatorView.isHidden =  false
                self.ActivityIndicator.startAnimating()

                let currentDate =  Date()
                let obj = self.userAct.filter("is_active = \(true)")

                var min = currentDate.minutes(from: obj[0].start_time)
                print(min)
                print(obj)
                
                if min == 0 {
                    min = 1
                }
                
                print("activityModel.liveActivityId - \(obj[0].user_id))")
                self.liveSessionModel.endActivityPatchCallWith(totalMinutes: min, liveActId: obj[0].user_id, completion: {[weak self](complete) in
                    if complete {
                        self!.endTimer()
                        self!.minutesLeftLabel.isHidden = true

                        
                        try! self!.realm.write {
                            obj[0].is_active = false
                        }
                        print(obj)
                    
                        //add time to selected activity and round up to nearest 15 here

                        self!.ActivityIndicator.stopAnimating()
                       self?.navigationController?.popViewController(animated: true)

                    }else{
                        print("error")
                        self!.endTimer()
                        self!.minutesLeftLabel.isHidden = true
                        print(obj)
                        
                        try! self!.realm.write {
                            obj[0].is_active = false
                        }
                        print(obj)
                        self!.ActivityIndicator.stopAnimating()
//                        self!.actview?.isHidden = false
//                        self!.actview.animShow()
                        self?.navigationController?.popViewController(animated: true)
//                        self!.dismiss(animated: true, completion: nil)
                    }

                })
                
            }))
            self.present(alert, animated: true)
        }
        
        
        
    }

    
    @IBAction func onExtendButtonTapped(_ sender: UIButton) {
        
        
        
    }
    
    
    
    func BeginTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
 
            if Date() < self.end {
                minutesLabel.text = timeRemainingString(finishDate: self.end)
                print(timeRemainingString(finishDate: self.end))

            }else {
            
                ActivityIndicator.startAnimating()
                userAct = userAct.filter("is_active = \(true)")
                
                
                liveSessionModel.getTotalLikeCount(activityId: userAct[0].user_id) { (bool, count) in
                    if bool {
                        self.ActivityIndicator.stopAnimating()

                        
                        self.likesCount = count
                        

                        self.removeLiveActivity()
                        self.performSegue(withIdentifier: "activitycomplete", sender: nil)
                    }else{
                        self.ActivityIndicator.stopAnimating()

                        print("response error")
                        
                        self.removeLiveActivity()
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                }
            
            
            self.minutesLeftLabel.isHidden = true
            endTimer()
        }
    }
    
    func endTimer() {
        timer.invalidate()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "activitycomplete" {
            if let vc = segue.destination as? ActivityCompleteViewController {
                vc.LikesCount = self.likesCount
                vc.minutes = String(Int(self.minutes))
                vc.activityName = liveSessionModel.myActivity
            }
        }
    }
    
    
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}



