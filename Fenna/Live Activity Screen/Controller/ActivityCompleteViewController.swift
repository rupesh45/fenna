//
//  ActivityCompleteViewController.swift
//  Fenna
//
//  Created by Rupesh Kadam on 12/04/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit
import ImageIO
import RealmSwift

class ActivityCompleteViewController: UIViewController {
    

    @IBOutlet weak var ActivityFeedbackLabel: UILabel!
    @IBOutlet weak var GifImageView: UIImageView!
    @IBOutlet weak var StartActivityButton: UIButton!
    @IBOutlet weak var ShareOnInstagramButton: UIButton!
    @IBOutlet weak var FennaLogoImageView: UIImageView!
    
    var schoolSelectionModel : SchoolSelectionModel!
    
    var LikesCount = 0
    
    let realm = AppSharedDetails.getRealmInstance()
    var userData : Results<UserModel>!
    
    var StoryView : InstaStoryView!
    
    var minutes  = ""
    var activityName = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        schoolSelectionModel = SchoolSelectionModel()
        userData = realm.objects(UserModel.self)
        
        schoolSelectionModel.SetupDesign(view: self.view, button: StartActivityButton)
        
        if LikesCount == 0 {
            ActivityFeedbackLabel.text = "You successfully completed your activity."
        }else if LikesCount == 1 {
            ActivityFeedbackLabel.text =  "\(LikesCount) friend liked your activity. Keep inspiring."
        }else{
            ActivityFeedbackLabel.text =  "\(LikesCount) friends liked your activity. Keep inspiring."
        }
        
        
        GifImageView.roundCornersWith(radius: 5)
        GifImageView.image = UIImage.gifImageWithName(name: "clappingman")
  
        
        StoryView = InstaStoryView.instanceFromNib() as? InstaStoryView
        print(userData[0].first_name)
        StoryView.UserNameLabel.text = userData[0].first_name
        StoryView.ActivityLabel.text = activityName
        StoryView.MinutesLabel.text = minutes
        
        ShareOnInstagramButton.underlineMyText()
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.hidesBackButton = true
        self.tabBarController?.tabBar.isHidden = true
    }
    

    @IBAction func StartActivityButton(_ sender: UIButton) {
        
        
        self.navigationItem.hidesBackButton = false
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    @IBAction func OnShareButtonTapped(_ sender: UIButton) {
        
        
//        StoryView = InstaStoryView.instanceFromNib() as? InstaStoryView
        FennaLogoImageView.image = UIImage(view: StoryView)
        FennaLogoImageView.shareOnInstagramStories()
        
    }
    
}


