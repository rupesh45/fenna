//
//  BadgesCollectionViewCell.swift
//  Fenna
//
//  Created by Rupesh Kadam on 26/07/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit

class BadgesCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var BadgeImageView: UIImageView!
    var userProModel : UserProfileViewModel!
    
    var heightSet = false

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
        
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        
        
    }
    
    
    
    
}

extension UIResponder {
    
    func next<T: UIResponder>(_ type: T.Type) -> T? {
        return next as? T ?? next?.next(type)
    }
}

extension UICollectionViewCell {
    
    var collectionView: UICollectionView? {
        return next(UICollectionView.self)
    }
    
    var indexPath: IndexPath? {
        
        return collectionView?.indexPath(for: self)
    }
}
