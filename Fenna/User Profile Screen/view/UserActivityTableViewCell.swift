//
//  UserActivityTableViewCell.swift
//  Fenna
//
//  Created by Rupesh Kadam on 28/05/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit

class UserActivityTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var ShapeImageView: UIImageView!
    @IBOutlet weak var ActivityNameLabel: UILabel!
    @IBOutlet weak var ActivityCountLabel: UILabel!
    @IBOutlet weak var MinutesLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        self.selectedBackgroundView = backgroundView
    }
    
}
