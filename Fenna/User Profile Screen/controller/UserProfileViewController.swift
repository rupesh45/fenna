//
//  UserProfileViewController.swift
//  Fenna
//
//  Created by Rupesh Kadam on 24/05/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit
import RealmSwift
import Charts

//do not delete comments

class UserProfileViewController: BackGroundViewController {
    
    
    @IBOutlet weak var UserProfileImageView: UIImageView!
    
    @IBOutlet weak var UserNameLabel: UILabel!
    
    @IBOutlet weak var SchoolGradeLabel: UILabel!
    
    @IBOutlet weak var FollowButton: UIButton!
    
    
    @IBOutlet weak var TodayButton: UIButton!
    @IBOutlet weak var WeekButton: UIButton!
    @IBOutlet weak var MonthButton: UIButton!
    @IBOutlet weak var pieChart: PieChartView!
    @IBOutlet weak var UserTableView: UITableView!
    
    @IBOutlet weak var badgeImageView: UIImageView!
    @IBOutlet weak var BadgesCollectionView: UICollectionView!
    @IBOutlet weak var FollowersCountLabel: UILabel!
    
    var userProfileModel : UserProfileViewModel!
    var timelinemodel : TimelineViewModel!
    
    var userName = ""
    var schoolAndGrade = ""
    var userId = ""
    
    
    var myUserId = AppUserDetails.getUserId()
    
    var entries : [PieChartDataEntry] = []
    
    var realm = AppSharedDetails.getRealmInstance()
    var followTable : Results<FollowerTable>!
    var userModel : Results<UserModel>!
    
    var datetoday = NetworkConstant.AnalysisMethods.today
    var dateweek = NetworkConstant.AnalysisMethods.week
    var datemonth = NetworkConstant.AnalysisMethods.month
    var refreshControl = UIRefreshControl()
    
    var userPopActUrl = ""
    
    let popAct = NetworkConstant.mainURL + NetworkConstant.UrlMethods.usergradeactivity + NetworkConstant.SubUrlMethods.analysis + "?" + "timezone=\(localTimeZoneName)"
    
    var mainUrl = ""
    var follCountFromTimeline = 0
    
    var followers = 0 {
       
        didSet {
            
            FollowersCountLabel.text = String(followers)
            NotificationCenter.default.post(name: .refreshTimeline, object: nil)
            
        }
    }
    
    
    var roleModel = true {
        
        didSet {
            
            if roleModel {
                if userId != myUserId{
                    FollowButton.isHidden = false
                    mainUrl = popAct
                }else{
                    
                }
                
                
            }else{
                
                FollowButton.isHidden = true
                mainUrl = popAct
                
                if followTable.count >= 1 {
                    unfollow { (complete) in
                        if complete {
                            print("rm unfollow")
                            self.followers -= 1
                        }else{
                            print("rm res err")
                        }
                    }
                }
                
            }
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(userId)
        print(myUserId)
        
        userPopActUrl = NetworkConstant.mainURL + NetworkConstant.UrlMethods.useractivity + NetworkConstant.SubUrlMethods.analysis + "?user_id=" + userId + "&" + "timezone=\(localTimeZoneName)"
        
        
        userProfileModel = UserProfileViewModel()
//        showTable(show: true)
        followTable = realm.objects(FollowerTable.self).filter("user_id = '\(userId)'")
        userModel = realm.objects(UserModel.self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(hideFollowButton), name: .hideFollowAndAnalytics, object: nil)
        
        print(followTable)
        
        
        //follow
        if followTable.count > 0 {
            print("isfollowed")
//            showTable(show: false)
            mainUrl = userPopActUrl
            userProfileModel.setUnFollowButton(button: FollowButton)
            getUserAnalysisData(type: datetoday)
        }else{
//            showTable(show: false)
            mainUrl = popAct
            getUserAnalysisData(type: datetoday)
        }
        
        //usermodel
//        if userModel[0].rolemodel {
//            
//            FollowButton.isHidden = false
//            
//        }else{
//            
//            FollowButton.isHidden = true
//        }
  
        UserNameLabel.text = userName
        SchoolGradeLabel.text = schoolAndGrade
        FollowersCountLabel.text = String(followers)
        
        userProfileModel.setupDesign(button: FollowButton, imageview: UserProfileImageView)
        
        UserTableView.delegate = self
        UserTableView.dataSource = self
        
        UserTableView.registerNib(nibName: "UserActivityTableViewCell", identifier: "cell")
        
//        pieChart.setupPieChart()
        
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        UserTableView.refreshControl = refreshControl
        
        //badge data
        
        
        BadgesCollectionView.delegate = self
        BadgesCollectionView.dataSource = self
        
        followers = follCountFromTimeline
        
        if userId == myUserId {
            FollowButton.isHidden = true
            mainUrl = popAct
        }else{
            
            if userModel[0].rolemodel {
                
                FollowButton.isHidden = false
                
            }else{
                
                FollowButton.isHidden = true
            }
//            FollowButton.isHidden = false
//            mainUrl = userPopActUrl
            
        }
        
        
        

        
        
    }
    
    @objc func onRefresh(refreshControl: UIRefreshControl) {
//        if TodayButton.alpha == 1 {
//            getUserAnalysisData(type: datetoday)
//        }else if WeekButton.alpha == 1 {
//            getUserAnalysisData(type: dateweek)
//        }else{
//            getUserAnalysisData(type: datemonth)
//        }
         print(mainUrl)
         getUserAnalysisData(type: datemonth)

        
    }
    
    @objc func hideFollowButton(_ noti : Notification){
        
        
        roleModel = !roleModel
        
        
    }
    
    func getUserAnalysisData(type : String) {
        userProfileModel.getUserAnalysisData(url: self.mainUrl, type: type, userId: userId) { (bool) in
            if bool {
                print("success")
//                self.entries = []
//                self.pieChart.notifyDataSetChanged()
//                self.pieChart.data = nil
//
//                for (index,min) in self.userProfileModel.UserPieChartMinutes.enumerated() {
//                    print(index,min)
//
//                    self.pieChartUpdate(index: index)
//                }
                
                self.refreshControl.endRefreshing()
                self.UserTableView.reloadData()
            }else{
                print("response error")
                self.refreshControl.endRefreshing()
            }
        }
    }
    
//    func pieChartUpdate (index : Int) {
//
//
//        let entry : PieChartDataEntry = PieChartDataEntry(value: Double(userProfileModel.getMinutes(index: index)), label: "")
//
//        entries.append(entry)
//        print("entries \(entries)")
//        let dataSet = PieChartDataSet(values: entries, label: "")
//        print(dataSet)
//
//
//
//        dataSet.valueTextColor = UIColor.clear
//        dataSet.valueLineWidth = 10
//        let data = PieChartData(dataSet: dataSet)
//        pieChart.data = data
//
//        //All other additions to this function will go here
//        dataSet.colors = [UIColor(hexString: "#8BE1FF"),UIColor(hexString: "#FF9A9E"),UIColor(hexString: "#00C51B"),UIColor(hexString: "#F98712"),UIColor(hexString: "#8859FE")]
//
//        pieChart.holeRadiusPercent = 0
//        pieChart.drawHoleEnabled = false
//        pieChart.backgroundColor = UIColor(hexString: "2E58E6")
//        pieChart.chartDescription?.textColor = UIColor.clear
//        pieChart.legend.textColor = UIColor.white
//
//        //This must stay at end of function
//        pieChart.notifyDataSetChanged()
//
//    }
    
    func showTable(show : Bool){
//        TodayButton.isHidden = show
//        WeekButton.isHidden = show
//        MonthButton.isHidden = show
//        pieChart.isHidden = show
        UserTableView.isHidden = show
    }
    
    func follow(completion : @escaping(Bool) -> Void) {
        self.userProfileModel.setUnFollowButton(button: self.FollowButton)
        timelinemodel.followUserWith(followerId: userId) {[weak self] (bool) in
            if bool {
//                self?.userProfileModel.setUnFollowButton(button: self!.FollowButton)
//                self?.showTable(show: false)
                self?.mainUrl = self!.userPopActUrl
                
                self?.getUserAnalysisData(type: self!.datetoday)
                
//                if self?.TodayButton.alpha == 1 {
//                    self?.getUserAnalysisData(type: self!.datetoday)
//                }else if self!.WeekButton.alpha == 1 {
//                    self?.getUserAnalysisData(type: self!.dateweek)
//                }else{
//                    self?.getUserAnalysisData(type: self!.datemonth)
//                }
                
                print(self?.followTable.count)
                
                completion(true)
            }else{
                print("response error")
                print("you are already following someone.")
                self?.userProfileModel.setFollowButton(button: self!.FollowButton)
                completion(false)
            }
        }
        
    }
    
    func unfollow(completion : @escaping(Bool) -> Void){
        self.userProfileModel.setFollowButton(button: self.FollowButton)
        
        timelinemodel.unfollowUser { (bool) in
            if bool {
//                self.showTable(show: true)
                self.mainUrl = self.popAct
                self.getUserAnalysisData(type: self.datetoday)
//                self.userProfileModel.setFollowButton(button: self.FollowButton)
                NotificationCenter.default.post(name: .refreshAnalytics, object: nil)
                completion(true)
            }else{
                print("response error")
                self.userProfileModel.setUnFollowButton(button: self.FollowButton)
                completion(false)
            }
        }
    }
    
    
    
    @IBAction func OnFollowButtonTapped(_ sender: UIButton) {
        
        if FollowButton.titleLabel?.text == "follow" {
            
            follow { (bool) in
                if bool{
                    print("follow success")
                    self.followers += 1
                }else{
                    self.followTable = self.realm.objects(FollowerTable.self)
                    
                    //show alert
                    AlertWithCompletion(title: "you are already following \(self.followTable[0].username).", message: "Do you want to unfollow \(self.followTable[0].username) and follow this user?", vc: self, completion: { (alert) in
                        self.unfollow(completion: { (bool) in
                            if bool {
                                self.follow(completion: { (bool) in
                                    if bool{
                                        print("success")
                                        self.followers += 1
                                    }else{
                                        print("error")
                                    }
                                })
                            }else{
                                
                            }
                        })
                    })
                }
            }
        }else{
            //unfollow here
            unfollow { (bool) in
                if bool{
                    print("unfollow success")
                    self.followers -= 1
                }else{
                    print("response error or server is closed.")
                }
            }

        }
 
    }
    
    func highlightTodayButton(buttonToHighlight button1 : UIButton,button2 : UIButton,button3 : UIButton) {
        
        if button1.alpha == 0.5 {
            button1.alpha = 1
            button2.alpha = 0.5
            button3.alpha = 0.5
        }
        
        
    }
    
    @IBAction func OnTodayButtonTapped(_ sender: UIButton) {
        
        highlightTodayButton(buttonToHighlight: sender, button2: WeekButton, button3: MonthButton)
        getUserAnalysisData(type: datetoday)
    }
    
    
    @IBAction func OnWeekButtonTapped(_ sender: UIButton) {
        highlightTodayButton(buttonToHighlight: sender, button2: TodayButton, button3: MonthButton)
        getUserAnalysisData(type: dateweek)
        
    }
    
    
    @IBAction func OnMonthButtonTapped(_ sender: UIButton) {
        highlightTodayButton(buttonToHighlight: sender, button2: WeekButton, button3: TodayButton)
        getUserAnalysisData(type: datemonth)
    }
    
    

}

extension UserProfileViewController : UITableViewDelegate,UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if TodayButton.alpha ==  1 {
//            return userProfileModel.getTodayActivitiesCount()
//        }else if WeekButton.alpha == 1 {
//            return userProfileModel.getWeekActiviesCount()
//        }else{
//            return userProfileModel.getMonthActivitiesCount()
//        }
        return userProfileModel.getActivitiesCount()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! UserActivityTableViewCell
        
        cell.ShapeImageView.image = userProfileModel.getShapeImage(index: indexPath.row)
        cell.ActivityNameLabel.text = userProfileModel.getActivityName(index: indexPath.row)
        cell.ActivityCountLabel.text = userProfileModel.getActivitiesCount(index: indexPath.row) + " activities"
        cell.MinutesLabel.text = userProfileModel.getTimeSpend(index: indexPath.row) + "\n" + "min"
        
        return cell
    }
  
    
}


extension UserProfileViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userProfileModel.BadgeImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! BadgesCollectionViewCell
        
        cell.userProModel = userProfileModel
        cell.BadgeImageView.image =  userProfileModel.getEmptyBadgeImages(index: indexPath.row)
        cell.BadgeImageView.alpha = 0.3
        
        if !cell.heightSet {
            userProfileModel.increaseImageHeight(imageView: cell.BadgeImageView)
            cell.heightSet = true
        }
        
        
        if userProfileModel.getBadgesCount() >= indexPath.row + 1{
            
            cell.BadgeImageView.image = userProfileModel.getBadgesAt(index : indexPath.row)
            cell.BadgeImageView.alpha = 1
     
        }else{

            
        }
        
  
        return cell
    }
    
    
    
}
