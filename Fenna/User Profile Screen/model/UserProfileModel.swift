//
//  UserProfileModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 27/05/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation

class UserProfileModel {
    
    static let sharedInstance = UserProfileModel()
    
    init() {
        
    }
    
    struct FollowedUserAnalysisData : Codable{
        let results : [FollowedUserData]
    }
    
    struct FollowedUserData : Codable {
        let id : Int
        let user_timespend : Int
        let name : String
        let user_activity_count : Int
    }
    
    struct BadgeModel : Codable {
        let results : [BadgeData]
    }
    
    struct BadgeData : Codable {
        let id : Int
        let image : String
    }
    
    
    enum Day : String,CaseIterable {
        
        case Today = "date=today"
        case Week = "date=week"
        case Month = "date=month"
        
        func dayType(for day: Day) -> String {
            switch day {
            case Day.Today:
                return "Today"
            case Day.Week:
                return "Week"
            case .Month:
                return "month"
            }
        }
        
    }
    
    enum UserActivitiesTypes : Int, CaseIterable {
        
        case My = 0
        case Group = 1
        case FollowedUser = 2
        
        func TagForActTypes() -> Int {
            switch self {
            case .My:
                return 0
            case .Group:
                return 1
            case .FollowedUser:
                return 2
                
            }
        }
        
        func TitleForActTypes() -> String {
            
            switch self {
                
            case .My:
                return "Your Activities"
            case .Group:
                return "Group Activities"
            case .FollowedUser:
                return "User Activities"
            }
            
        }
        
    }

    
    
}
