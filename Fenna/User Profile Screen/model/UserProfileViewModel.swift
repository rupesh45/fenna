//
//  UserProfileViewModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 24/05/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireImage
import Charts

class UserProfileViewModel {
    
    init() {
        
    }
    
    var userActivityData = [UserProfileModel.FollowedUserData]()
    var UserPieChartMinutes = [Int]()
    
    var userTodayAnalysisData = [UserProfileModel.FollowedUserData]()
    var userWeekAnalysisData = [UserProfileModel.FollowedUserData]()
    var userMonthAnalysisData = [UserProfileModel.FollowedUserData]()
    
    var height : CGFloat = 0
    
    var badgeData = [UserProfileModel.BadgeData]()
    
    var BadgeImages : [UIImage?] = [UIImage(named: "s2"),UIImage(named: "s4"),UIImage(named: "s6"),UIImage(named: "s8"),UIImage(named: "s10")]
    
    var EmptyBadgeImages : [UIImage?] = [UIImage(named: "s1"),UIImage(named: "s3"),UIImage(named: "s5"),UIImage(named: "s7"),UIImage(named: "s9")]
    
    func getUserAnalysisData(url : String,type : String,userId : String,completion : @escaping(Bool) -> Void){
        
//        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.useractivity + NetworkConstant.SubUrlMethods.analysis + "?user_id=" + userId
        
        let url = url
        
        NetworkModel.sharedInstance.startNetworkGetCallWithHeader(URL: url) { (response) in
            if let status = response.response?.statusCode {
                print(status)
                switch status{
                case 200...300:
                    print("response success")
                    print(response.result.value)
                    
                    if let data = response.data {
                        do{
                            let results = try JSONDecoder().decode(UserProfileModel.FollowedUserAnalysisData.self, from: data)
                            print(results)
                            self.userActivityData = results.results
                            self.UserPieChartMinutes = []
                            
                            for data in self.userActivityData {
                                self.UserPieChartMinutes.append(data.user_timespend)
                            }
//                            if type == NetworkConstant.AnalysisMethods.today {
//                                print("today data")
//                                self.userTodayAnalysisData = results.results
//
//                            }else if type == NetworkConstant.AnalysisMethods.week{
//                                print("week data")
//                                self.userWeekAnalysisData = results.results
//                            }else{
//                                print("month data")
//                                self.userMonthAnalysisData = results.results
//                            }
                        }catch{
                            print("unable to parse")
                        }
                    }
                    
                    
                    completion(true)
                case 400...600:
                    
                    print("response error")
                    print(response.result.value)
                    completion(false)
                    
                default:
                    print("response error default")
                    
                    completion(false)
                }
            }
        }
        
    }
    
    func getAllUserBadges(completion : @escaping(Bool) -> Void) {
        
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.badge
        
        NetworkModel.sharedInstance.startNetworkGetCallWithHeader(URL: url) { (response) in
            if let status = response.response?.statusCode {
                print(status)
                switch status{
                case 200...300:
                    print("response success")
                    print(response.result.value)
                    
                    if let data = response.data {
                        do{
                            let results = try JSONDecoder().decode(UserProfileModel.BadgeModel.self, from: data)
                            print(results)
                            self.badgeData = results.results

         
                        }catch{
                            print("unable to parse")
                        }
                    }
                    
                    
                    completion(true)
                case 400...600:
                    
                    print("response error")
                    print(response.result.value)
                    completion(false)
                    
                default:
                    print("response error default")
                    
                    completion(false)
                }
            }
        }
        
        
    }
    
    func activityShapeImage(index: Int) -> UIImage {
        var images : [UIImage?] = [UIImage(named: "shape1"),UIImage(named: "shape2"),UIImage(named: "shape3"),UIImage(named: "shape4"),UIImage(named: "shape5")]
        
        return images[index]!
        
    }
    
    //badges
    
    func getBadges(completion : @escaping(UIImage) -> Void) {
        
        if let urlstring = badgeData[0].image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed){
            if let url = URL(string: urlstring) {
                GetActivityImage(withUrl: url) { (img) in
                    completion(img)
                }
            }
        }
    
        
        
    }
    
    private func GetActivityImage(withUrl :URL, completion : @escaping(UIImage) -> Void ){
        var activityImage = UIImage()
        Alamofire.request(withUrl).responseImage { (response) in
            if let img = response.result.value {
                activityImage = img
                completion(activityImage)
            }
        }
        
    }
    
    //collectionview data
    
    func getEmptyBadgeImages(index : Int) -> UIImage {
        
        if let img = EmptyBadgeImages[index] {
            
            return img
        }else{
            
            return UIImage()
            
        }
        
    }
    
    func getBadgesAt(index : Int) -> UIImage {
        
        if let img = BadgeImages[index] {
            return img
        }else{
            
            return UIImage()
            
        }
        
    }
    
    func increaseImageHeight(imageView : UIImageView) {
        
        imageView.frame.size.height = imageView.frame.size.height + height
        imageView.frame.origin.y = imageView.frame.origin.y - height
        
        height = height + 5
        
    }
    
    func getBadgesCount() -> Int {
        
        return 1
        
    }
    
    
    
    
    //tableview data
    func getActivitiesCount() -> Int{
        return userActivityData.count
    }
    
    func getActivityName(index : Int) -> String {
        return userActivityData[index].name
    }
    
    func getShapeImage(index : Int) -> UIImage{
        return activityShapeImage(index: index)
    }
    
    func getActivitiesCount(index : Int) -> String {
        return String(userActivityData[index].user_activity_count)
    }
    
    func getTimeSpend(index : Int) -> String{
        return String(userActivityData[index].user_timespend)
    }
    
    func getMinutes(index : Int) -> Int {
       
        return self.UserPieChartMinutes[index]
        
        
    }
    
    
    //other stuff , no use
    func getTodayActivitiesCount() -> Int {
        
        return userTodayAnalysisData.count
    }
    
    func getWeekActiviesCount() -> Int{
        
        return userWeekAnalysisData.count
    }
    
    func getMonthActivitiesCount() -> Int{
        
        return userMonthAnalysisData.count
    }
    
    func getTodayActivityName(index : Int) -> String {
        return userTodayAnalysisData[index].name
    }
    
    
    
    //setup design
    
    func setupDesign(button : UIButton,imageview : UIImageView) {
        imageview.roundCorners()

        imageview.layer.borderColor = UIColor(hexString: "#FDE891").cgColor
        imageview.layer.borderWidth = 3
        
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = button.frame.height / 2
    }
    
    func setFollowButton(button : UIButton){
        button.setTitle("follow", for: .normal)
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor.white, for: .normal)
    }
    
    func setUnFollowButton(button : UIButton) {
        button.setTitle("Unfollow", for: .normal)
        button.backgroundColor = UIColor.white
        button.setTitleColor(UIColor.black, for: .normal)
    }
    
    
    
}


