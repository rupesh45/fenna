//
//  CurrentGoalsModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 18/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import UIKit

class CurrentGoalsModel {
    
    static let sharedInstance = CurrentGoalsModel()
    
    let timelinemodel = TimelineViewModel.sharedInstance
    let schoolSelectionModel = SchoolSelectionModel.sharedInstance
    
    init() {
        
    }
    
    func getCurrentGoalsCount() -> Int {
        return timelinemodel.getTimelineCount()
    }
    
//    func getCurentGoalsImages(index : Int) -> UIImage {
//        return timelinemodel.getTimelineImage(index: index)
//    }
    
    func SetupButtonDesign(button : UIButton){
        setupButton(button: button)
    }
    
    private func setupButton(button: UIButton){
        button.layer.cornerRadius = 30
        button.layer.masksToBounds = true
        schoolSelectionModel.setGradientBackgroundButton(view: button)
    }
    
    
    
    
    
}
