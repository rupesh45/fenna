//
//  CurrentGoalsViewController.swift
//  Fenna
//
//  Created by Rupesh Kadam on 18/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit
import SwipeCellKit

class CurrentGoalsViewController: BackGroundViewController {

    @IBOutlet weak var CurrentGoalsTableView: UITableView!
    @IBOutlet weak var AddGoalButton: UIButton!
    
   
    var currentGoals : CurrentGoalsModel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        CurrentGoalsTableView.delegate = self
        CurrentGoalsTableView.dataSource = self
        currentGoals.SetupButtonDesign(button: AddGoalButton)
        CurrentGoalsTableView.registerNib(nibName: "AtivityFeedTableViewCell", identifier: "cell")
        
    }
    
}

extension CurrentGoalsViewController : UITableViewDelegate,UITableViewDataSource,SwipeTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        guard orientation == .right else {
            return nil
        }
        
        print("yo")
        let deleteAction = SwipeAction(style: .default, title: "Delete") { action, indexPath in
            // handle action by updating model with deletion
            print("deleted")
            
        }
        
        deleteAction.backgroundColor = UIColor.clear
        
        // customize the action appearance
        deleteAction.image = UIImage(named: "delete")
        
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var option = SwipeTableOptions()
        option.expansionStyle = SwipeExpansionStyle.selection
        option.transitionStyle = SwipeTransitionStyle.drag
        
        option.buttonPadding = 5
        option.backgroundColor = UIColor.clear
        return option
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentGoals.getCurrentGoalsCount()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! AtivityFeedTableViewCell
//        cell.ActivityImageView.image = currentGoals.getCurentGoalsImages(index: indexPath.row)
        cell.delegate = self
        //        cell.UserCurrentActivityLabel.text = timelineModel.getTimelineUserNameAndActivity(index: indexPath.row)
        return cell
    }
    
    
}
