//
//  MainModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 01/04/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import RealmSwift



class UserModel : Object {
    
    @objc dynamic var user_id = ""
    @objc dynamic var first_name = ""
    @objc dynamic var last_name = ""
    @objc dynamic var location = ""
    @objc dynamic var school = ""
    @objc dynamic var grade = ""
    @objc dynamic var userImagePath = ""
    @objc dynamic var rolemodel = true
    @objc dynamic var schoolid = 0
    @objc dynamic var gradeid = 0
    
    
    override static func primaryKey() -> String? {
        return "user_id"
    }
    
    
}

class SchoolModel : Object {
    
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}



class Activity : Object {
    
    @objc dynamic var user_id = 0
    @objc dynamic var activity_id = 0
    @objc dynamic var activity_image = ""
    @objc dynamic var name = ""

    override static func primaryKey() -> String? {
        return "activity_id"
    }
    
    
}

class userActivityTable: Object {
    
     @objc dynamic var user_id = 0
     @objc dynamic var activity_id = 0
     @objc dynamic var name = ""
     @objc dynamic var user_activity_id = 0
     @objc dynamic var duration = 0
     @objc dynamic var start_time = Date()
     @objc dynamic var end_time = Date()
     @objc dynamic var is_active = false
    
    override static func primaryKey() -> String? {
        return "user_activity_id"
    }
}

class TimelineTable : Object {
    
    @objc dynamic var id = 0
    @objc dynamic var user_id = 0
    @objc dynamic var activity_id = 0
    @objc dynamic var activity_name = ""
    @objc dynamic var user_name = ""
    @objc dynamic var user_image = ""
    @objc dynamic var start_time = Double()
    @objc dynamic var end_time = Double()
    @objc dynamic var is_active = false
//    @objc dynamic var duration = 0
    @objc dynamic var pre_duration = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

class FollowerTable : Object {
    
    @objc dynamic var id = 0
    @objc dynamic var my_id = ""
    @objc dynamic var user_id = ""
    @objc dynamic var username = ""
    @objc dynamic var isfollowed = false
    
    override static func primaryKey() -> String? {
        return "user_id"
    }
}
