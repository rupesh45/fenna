//
//  AppSettingsViewController.swift
//  Fenna
//
//  Created by Rupesh Kadam on 08/08/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit
import RealmSwift

class AppSettingsViewController: BackGroundViewController {
    
    
    @IBOutlet weak var DesignView: UIView!
    @IBOutlet weak var RoleModelSwitch: UISwitch!
    @IBOutlet weak var InviteButton: UIButton!
    @IBOutlet weak var FeedbackButton: UIButton!
    
    var appSettingsModel : AppSettingsViewModel!
    var userModel : Results<UserModel>!
    
    let realm = AppSharedDetails.getRealmInstance()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        appSettingsModel = AppSettingsViewModel()
        
        
        appSettingsModel.setupDesignView(view: DesignView)
        RoleModelSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        
        userModel = realm.objects(UserModel.self)
        
        if userModel[0].rolemodel {
            
            RoleModelSwitch.isOn = true
            
        }else{
            
            RoleModelSwitch.isOn = false
        }
        
    }
    
    @IBAction func OnInviteButtonTapped(_ sender: UIButton) {
        
        let myWebsite = URL(string:"https://apps.apple.com/us/app/finna-time/id1481058755")
        let shareAll = [myWebsite]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func ShowPrivacyPolicyButton(_ sender: UIButton) {
        
        let url = URL(string:"https://app.termly.io/document/privacy-policy/54e08d06-19c7-4e6a-912f-c1035a030d6f")!
        
        UIApplication.shared.open(url)
        
        
        
    }
    
    @IBAction func OnFeedbackButtonTapped(_ sender: UIButton) {
        
        let url = URL(string:"https://finnatime.com/school.html")!
        
        UIApplication.shared.open(url)
        
    }
    
    
    @IBAction func OnSwitchButtonTapped(_ sender: UISwitch) {
        

        
        
        
        if sender.isOn {
            
            try! realm.write {
                userModel[0].rolemodel = true
            }
            
            NotificationCenter.default.post(name: .hideFollowAndAnalytics, object: nil)
            
            
        }else{
            
            let alert  = UIAlertController(title: "", message: "Your peer won't be able to follow you and you cannot follow your peers", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { (alert) in
                
                try! self.realm.write {
                    self.userModel[0].rolemodel = false
                }
                
                NotificationCenter.default.post(name: .hideFollowAndAnalytics, object: nil)
                
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (alert) in
                
                sender.isOn = true
                
            }))
            
            self.present(alert, animated: true)
            
            
            
        }
        
        
    }
    
    
    
    
}
