//
//  AppSettingsViewModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 08/08/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import UIKit

class AppSettingsViewModel {
    
    init() {
        
    }
    
    func setupDesignView(view : UIView) {
        view.addGradientBackground(firstColor: UIColor(hexString: "#3E31D5"), secondColor: UIColor(hexString: "#4E18BB"))
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
    }
    
    
}
