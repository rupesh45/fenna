//
//  Constants.swift
//  Fenna
//
//  Created by Rupesh Kadam on 05/04/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation


struct Constants {

    struct UserDefaultKeys{
        static let userId = "UserId"
        static let previousUserId = "previousUserId"
        static let jwtToken = "JWTToken"
        static let fcmToken = "fcmToken"
        static let mobileNumber = "mobileNumber"
        static let userupdate = "userupdate"
    }
    
    struct PerformSegue {
        static let loginSegue = "register"
        static let homeSegue = "newActivity"
        static let mobregsegue = "mobregister"
    }
}
