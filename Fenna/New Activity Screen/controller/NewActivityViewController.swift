//
//  NewActivityViewController.swift
//  Fenna
//
//  Created by Rupesh Kadam on 11/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import RealmSwift
import AlamofireImage
import Alamofire



class NewActivityViewController: BackGroundViewController {
    
    @IBOutlet weak var AddButton: UIButton!
    @IBOutlet weak var ActivityCollectionView: UICollectionView!
    @IBOutlet weak var MinutesPickerView: UIPickerView!
    @IBOutlet weak var SearchButton: UIButton!
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    
    var TempSchoolSelectionModel : SchoolSelectionModel!
    var activityModel : MainActivityModel!
    var analysisModel : AnalysisViewModel!
    var selectedTime = 15
    var selectedActArray = [String]()
    var selectedImageArray = [UIImage]()
    
    var activityData : Results<Activity>!
    var userAct : Results<userActivityTable>!
    var refreshControl = UIRefreshControl()
    
    var AllActivityImageArray : [UIImage] = []
    
    var startDate = Date()
    let realm = AppSharedDetails.getRealmInstance()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         
        
        activityData = realm.objects(Activity.self)
        userAct = realm.objects(userActivityTable.self)
        activityIndicator.setupActivityIndicator()
        
        if Reachability.isConnectedToInternet{
            getActivityData()
        }else{
            print("no Internet")
            errorAlert(title: "No internet", message: "Please check your internet connection", vc: self)
        }
        
        

        // Do any additional setup after loading the view.
        ActivityCollectionView.delegate = self
        ActivityCollectionView.dataSource = self
        MinutesPickerView.delegate = self
        MinutesPickerView.dataSource = self
        
        ActivityCollectionView.registerNib(nibName: "ActivityCollectionViewCell", identifier: "cell")
        
        TempSchoolSelectionModel.SetupDesign(view: self.view, button: AddButton)
        
        
        activityModel.rotatePickerView(pickerView: MinutesPickerView)
        
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        ActivityCollectionView.refreshControl = refreshControl
        
        
        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        liveActivityCheck()
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        print(self.view.frame.height)
//        print(self.view.frame.width)
        activityModel.cellLayout(view: self.view, collectionView: ActivityCollectionView)
        
    }
    
    func getActivityData(){
        activityIndicator.startAnimating()
        activityModel.getActivityData {[weak self](complete) in
            if complete{
                self!.activityModel.selectedActivity.selectedActivityName = ""
                self!.selectedActArray = []
                self!.activityIndicator.stopAnimating()
                self!.ActivityCollectionView.reloadData()
                self!.refreshControl.endRefreshing()
                
                

                
            }else{
                self!.activityIndicator.stopAnimating()
                self!.refreshControl.endRefreshing()
                errorAlert(title: "", message: "unable to decode data", vc: self!)
                print("unable to decode data")
            }
            
        }
    }
    
    @objc func onRefresh(refreshControl: UIRefreshControl) {
        
        if Reachability.isConnectedToInternet{
            getActivityData()
        }else{
            print("no Internet")
            errorAlert(title: "No internet", message: "Please check your internet connection", vc: self)
            refreshControl.endRefreshing()
            
        }
    }
    
    func liveActivityCheck(){
        if userAct.count == 0 {
            
        }else if userAct.count > 0 {
            let liveAct = userAct.filter("is_active = \(true)")
            if liveAct.count > 0 {
                activityIndicator.stopAnimating()
                print("live act id \(liveAct[0].activity_id)")
                    self.performSegue(withIdentifier: "live", sender: nil)
            }
            
        }
    }
 
    
    @IBAction func onSeachButtonTapped(_ sender: UIButton) {
        
        performSegue(withIdentifier: "search", sender: nil)
    }
    

    
    @IBAction func onStartActivityButtonTapped(_ sender: UIButton) {
        if activityModel.selectedActivity.selectedActivityName == "" {
            let alert = UIAlertController(title: "please select the activity", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            }))
            self.present(alert, animated: true)
            
        }else {

            print("starting activity with id \(activityModel.selectedActivity.selectedActivityId) and selected time - \(selectedTime)")
            activityIndicator.startAnimating()
            activityModel.getDateWith(activityId: activityModel.selectedActivity.selectedActivityId, Minutes: selectedTime) {(complete) in
                if complete{
                    
                    
                    let obj = userActivityTable()
                    let userid = self.activityModel.liveActivityId
                    let act_id = self.activityModel.selectedActivity.selectedActivityId
                    let useract_id = incrementID(userActivityTable.self)
                    let name = self.activityModel.selectedActivity.selectedActivityName
                    let start_time = Date()
                    let end_time = self.activityModel.epochToDate()
                    let duration = self.selectedTime
                    
                    obj.user_id = userid
                    obj.activity_id = act_id
                    obj.user_activity_id = useract_id
                    obj.name = name
                    obj.start_time = start_time
                    obj.end_time = end_time
                    obj.duration = duration
                    obj.is_active = true
                    
                    print(obj)
                    
                    try! self.realm.write {
                        self.realm.add(obj)
                    }
                    self.activityIndicator.stopAnimating()
                    self.performSegue(withIdentifier: "live", sender: nil)
                }else{
                    self.activityIndicator.stopAnimating()
                    errorAlert(title: "", message: "error while starting an activity", vc: self)
                    print("error while starting an activity")
                }
            }
            
            
        }
        
    }


    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "live" {
            if let vc = segue.destination as? LiveSessionViewController {
                
                let obj = userAct.filter("is_active = \(true)")
                
                vc.schoolSelectionModel = self.TempSchoolSelectionModel
                vc.liveSessionModel = LiveSessionModel()
                vc.liveSessionModel.myActivity = obj[0].name
                vc.activityModel = self.activityModel
                
                
                vc.end = obj[0].end_time
                vc.activityId = obj[0].activity_id
                vc.minutes = Float(obj[0].duration)
                
                
            }
            
        }else if segue.identifier == "search" {
            if let vc = segue.destination as? SearchActivitiesViewController {
               vc.activityModel = self.activityModel
                vc.searchActivityModel = SearchActivityModel()
                vc.delegate = self
//                vc.actImageFilter = []
////                vc.actImageFilter = AllActivityImageArray
//                vc.actImageFilter = AllActivityImageArray
                
            }
            
        }
 
        
    }
    
}


extension NewActivityViewController : UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        pickerView.subviews.forEach({
            
            $0.isHidden = $0.frame.height < 1.0
        })
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return activityModel.getMinutesCount()
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 100
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        return activityModel.returnPickerLabel(row: row)

    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedTime = Int(activityModel.minutes[row])!
        print(selectedTime)
    }
    
    
    
    
}


extension NewActivityViewController : SelctedActivityFromSearch {
    func onSelectedActivity(actId: Int, activity: String, image: UIImage) -> Bool {
        
        selectedActArray = []
        selectedImageArray = []
        activityModel.selectedActivity.selectedActivityName = activity
        activityModel.selectedActivity.selectedActivityId = actId
        selectedActArray.append(activity)
        selectedImageArray.append(image)
        ActivityCollectionView.reloadData()
        return true
    }
    
    
    
    
    
}

extension NewActivityViewController : UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if selectedActArray.first != nil {
            return selectedActArray.count
        }else {
            return activityModel.getActivityCount()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ActivityCollectionViewCell
        if let a = selectedActArray.first{
            cell.ActivityNameLabel.text = a
            cell.ActivityImageView.image = selectedImageArray.first
        }else{
//            self.AllActivityImageArray = []
            cell.ActivityNameLabel.text = activityModel.getActivityName_MainActModel(index: indexPath.row)
//            activityModel.getActivityImage(index: indexPath.row, completion: { (image) in
//                cell.ActivityImageView.image = image
////                self.AllActivityImageArray.append(image)
//            })
            let url = activityModel.mainActivityData[indexPath.row].activity_url
            cell.ActivityImageView.loadImageFromurl(urlString: url)
            
//            cell.ActivityImageView.image = activityModel.getCleanImage(index: indexPath.row)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let a = selectedActArray.first {
//            activityModel.selectedActivity.selectedActivityName = a
        }else{
           activityModel.selectedActivity.selectedActivityName = activityModel.getActivityName_MainActModel(index: indexPath.row)
            activityModel.selectedActivity.selectedActivityId = activityModel.mainActivityData[indexPath.row].id
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let lastItem = activityModel.mainActivityData.count - 1
        if indexPath.row == lastItem {
            print("request more data")
            loadMoreActs()
        }
    }
    
    func loadMoreActs() {
        activityModel.loadMoreActivities { (bool) in
            if bool {
                self.ActivityCollectionView.reloadData()
            }else{
                print("response error loading more activities.")
            }
        }
    }
    
    
}


