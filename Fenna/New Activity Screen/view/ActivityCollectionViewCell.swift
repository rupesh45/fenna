//
//  ActivityCollectionViewCell.swift
//  Fenna
//
//  Created by Rupesh Kadam on 11/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit

class ActivityCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var ActivityImageView: UIImageView!
    @IBOutlet weak var ActivityNameLabel: UILabel!
    
    var selectedActivity = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.cornerRadius = 8
        self.layer.masksToBounds = true
//        setGradientBackground(view: self)
    }
    
    override var isSelected: Bool {
        willSet{
            super.isSelected = newValue
            if newValue
            {
                self.layer.borderColor = UIColor.white.cgColor
                self.layer.borderWidth = 2
                ActivityNameLabel.alpha = 1
                print("selected")
            }
            else
            {
                self.layer.borderColor = UIColor.clear.cgColor
                self.layer.borderWidth = 0
                ActivityNameLabel.alpha = 0.5
                print("cant select")
            }
        }
    }
    
    private func setGradientBackground(view : UIView) {
        let colorTop =  UIColor(hexString: "#3453E7").cgColor
        let colorBottom = UIColor(hexString: "#3D3BCF").cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = view.bounds
        
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    


}
