//
//  Activity.swift
//  Fenna
//
//  Created by Rupesh Kadam on 28/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import UIKit

class ActivityModel {
    
    struct MainActivityData : Codable {
//        let count : Int
        let next : String?
//        let previous : nil
        let results : [Activity]
    }
    
    struct Activity: Codable {
        let id : Int
        let name : String
//        let action : String
        let activity_url : String
        
        init(id: Int,name: String,action: String,activity_url : String) {
            self.id = id
            self.name = name
//            self.action = action
            self.activity_url = activity_url
        }

    }
    
    struct StartedLiveAtivityResponse: Codable {
//        let action : String
        let id : Int
        let activity : Int
        let duration : Int
        let start_time : Double
        let end_time : Double
        
    }
    
    
    
    
}
