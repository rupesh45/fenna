//
//  NewActivityModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 12/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import RealmSwift
import AlamofireImage

class MainActivityModel {
    
    static let sharedInstance = MainActivityModel()
    
    struct selectedActivityStruct {
        
        var selectedActivityName : String = ""
        var selectedActivityId : Int = 0
        var selectedActivityImage : UIImage = UIImage()
    }
    
    var minutes = ["15","30","60","90"]
    var mainActivityData : [ActivityModel.Activity] = []
    var selectedActivity = selectedActivityStruct()
    var completionHandler : ((Bool) -> ())? = nil
    
    var endDateEpoch : Double!
    
    var liveActivityId = Int()
    
    

    
    var nextApiString : String?

    
    init() {
        
    }
    
     func getActivityData(completion :@escaping (Bool) -> Void) {
        
//        allImages = []
        nextApiString = nil
        
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.activity + NetworkConstant.SubUrlMethods.list
        NetworkModel.sharedInstance.startNetworkGetCallWithHeader(URL: url) { (response) in
            if let status = response.response?.statusCode {
                print(status)
                switch status{
                    case 200...300 :
                        print("response succes")
                        print(response.result.value)
                        if let data = response.data {
                            do {
                                let act = try JSONDecoder().decode(ActivityModel.MainActivityData.self, from: data)
    
                                self.mainActivityData = act.results
                                
                                if let nextApi = act.next {
                                    self.nextApiString = nextApi
                                }
                                
                                print(self.mainActivityData)
                                completion(true)

                            }catch{
                                completion(false)
                                print("unable to get data or wrong data")
                            }
                            
                        }
                    
                    case 400...600:
                        completion(false)
                        print("error with status \(status)")
                    default:
                        completion(false)
                        print("default \(status)")
                    }
            }
            
        }

    }
    
  
    func getDateWith(activityId : Int, Minutes : Int,completion :@escaping (Bool) -> Void){
        
        completionHandler = completion
        
        let packetCreator = Packets()
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.activity + NetworkConstant.SubUrlMethods.user
        NetworkModel.sharedInstance.startNetworkPostCall(withURL: url, andParams: packetCreator.createStartActivityPacketWith(Activity_id: activityId, duration: Minutes)  , onCompletion: onCompletion)
        
    }
    
    func onCompletion(responseData : DataResponse<Any>){
        if let status = responseData.response?.statusCode {
            print(status)
            switch status {
                case 200...300 :
                print("response success")
                if let data = responseData.data {
                    do {
                        let json = try JSONDecoder().decode(ActivityModel.StartedLiveAtivityResponse.self, from: data)
                        print(json)
                        self.liveActivityId = json.id
                        self.endDateEpoch = json.end_time
                        print(json.end_time)
                        print(self.liveActivityId)

          
                        if let callback = self.completionHandler {
                            callback(true)
                        }
                    }catch{
                        print("unable to decode data")
                        if let callback = self.completionHandler {
                            callback(false)
                        }
                    }
                }
                
                case 400...600 :
                    if let callback = self.completionHandler {
                        callback(false)
                    }
                    print("error with status code \(status)")
                
            default:
                if let callback = self.completionHandler {
                    callback(false)
                }
                print("default \(status)")
            }
            
            
        }
    }
    
    
    func loadMoreActivities(completion : @escaping(Bool) -> Void) {
        
        if nextApiString != nil {
            
            if let url = nextApiString {
                
                NetworkModel.sharedInstance.startNetworkGetCallWithHeader(URL: url) { (response) in
                    if let status = response.response?.statusCode {
                        print(status)
                        switch status {
                        case 200...300 :
                            print("response success")
                            print(response.result.value)
                            if let data = response.data {
                                
                                do {
                                    let json = try JSONDecoder().decode(ActivityModel.MainActivityData.self, from: data)
                                    
                                    
                                    for data in json.results {
                                        self.mainActivityData.append(data)
                                    }
                                    
                                    if let nextApi = json.next {
                                        self.nextApiString = nextApi
                                    }else{
                                        self.nextApiString = nil
                                    }
                                    
                                    
                                    completion(true)
                                    
                                }catch{
                                    print("unable to decode data")
                                    
                                    completion(false)
                                    
                                }
                            }
                            
                        case 400...600 :
                            
                            completion(false)
                            
                            print("error with status code \(status)")
                            
                        default:
                            
                            completion(false)
                            
                            print("default \(status)")
                        }
                        
                        
                    }
                }
                
            }
            
            
        }
        
        
        
    }

    func epochToDate() -> Date{
        let endDate = Date(timeIntervalSince1970: endDateEpoch)
        print(endDate)
        return endDate
    }
    

    
    func getMinutesCount() -> Int {
        return minutes.count
    }
    
    func getMinutesText(index : Int) -> String {
        return minutes[index]
    }
    
    func getActivityName_MainActModel(index : Int) -> String {
        return mainActivityData[index].name
    }
    
    func getActivityId_MainActModel(index : Int) -> Int {
        
        return mainActivityData[index].id
        
    }
    
    func getActivityCount() -> Int {
        return mainActivityData.count
    }
    
    func getActivityImage(index : Int,completion : @escaping(UIImage) -> Void) {
        
        let imgUrlString = mainActivityData[index].activity_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        if let imgUrl = URL(string: imgUrlString) {
            GetActivityImage(withUrl: imgUrl) { (image) in
                completion(image)
            }
        }
        
        
        
    }


    
    private func GetActivityImage(withUrl :URL, completion : @escaping(UIImage) -> Void ){
        var activityImage = UIImage()
        Alamofire.request(withUrl).responseImage { (response) in
            if let img = response.result.value {
                activityImage = img
                completion(activityImage)
            }
        }
        
    }
    
    
    
    
    //design
    func rotatePickerView(pickerView : UIPickerView) {
        let y = pickerView.frame.origin.y
        let x = pickerView.frame.origin.x
        
        pickerView.transform = CGAffineTransform(rotationAngle: -90 * (.pi / 180 ))
        pickerView.frame = CGRect(x: x, y: y, width: pickerView.frame.height , height: pickerView.frame.width)
    }
    
    func returnPickerLabel(row : Int) -> UIView{
        let label = UILabel()
        label.font = UIFont(name: "SF Pro Display ", size: 80)
        label.font = UIFont.systemFont(ofSize: 70, weight: .bold)
        label.minimumScaleFactor = 0.6
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.transform = CGAffineTransform(rotationAngle: 90 * (.pi / 180 ))
        
        label.text = minutes[row]
        
        return label
    }
    
    func  cellLayout(view : UIView,collectionView : UICollectionView){
        let layout = UICollectionViewFlowLayout()
        let cellSize = CGSize(width: view.frame.width / 2.5 , height: view.frame.height / 5.58)
        layout.itemSize = cellSize
        layout.minimumLineSpacing = 11
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        collectionView.collectionViewLayout = layout
        collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    
}
