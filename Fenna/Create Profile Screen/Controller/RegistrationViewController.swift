//
//  RegistrationViewController.swift
//  Fenna
//
//  Created by Rupesh Kadam on 05/04/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire

class RegistrationViewController: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var UserProfileImageView: UIImageView!
    
    @IBOutlet weak var FirstNameTextField: UITextField!
    
    @IBOutlet weak var LastNameTextField: UITextField!
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    
    @IBOutlet weak var ImageDesignView: UIView!
    
    @IBOutlet weak var DoneButton: UIButton!
    
    var imagePicker = UIImagePickerController()
    
    var schoolSelectionModel : SchoolSelectionModel!
    var registrationModel : RegistrationModel!
    
    var selectedSchool : String!
    var selectedLocation : String!
    var selectedGrade : String!
    var selectedSchoolId : Int!
    var selectedGradeId : Int!
    
    var clickedTextField = UITextField()
    
    var fcmtoken = String()
    
    var userItem : UserItem!
    var schoolSelectionItem : SchoolSelectionItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fcmtoken = AppUserDetails.getFcmToken()
        
        activityIndicator.setupActivityIndicator()
        
        registrationModel = RegistrationModel.sharedInstance
        schoolSelectionModel = SchoolSelectionModel.sharedInstance
        
        FirstNameTextField.delegate = self
        LastNameTextField.delegate = self
        schoolSelectionModel.SetupDesign(view: self.view, button: DoneButton)
//        registrationModel.setup(textFields: [FirstNameTextField,LastNameTextField])
        
        FirstNameTextField.setupPlaceHoder(placeholder: "Full Name", color: UIColor.white)
//        LastNameTextField.setupPlaceHoder(placeholder: "Last Name")
        FirstNameTextField.borderStyle = .roundedRect
        
//        ImageDesignView.layer.cornerRadius = 8
//        ImageDesignView.layer.masksToBounds = true
//        ImageDesignView.layer.borderWidth = 2
//        ImageDesignView.layer.borderColor = UIColor(hexString: "#FDE891").cgColor
        
        registrationModel.setupImageView(view: ImageDesignView)

        
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.selectImage(_:)))
        UserProfileImageView.isUserInteractionEnabled = true
        UserProfileImageView.addGestureRecognizer(tap)
        imagePicker.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil);
        
//        UserProfileImageView.layer.borderColor = UIColor(hexString: "#792DFE").cgColor
//        UserProfileImageView.layer.borderWidth = 20
        
//        registrationModel.setDelegate(delegate: self)
//        print(schoolSelectionItem)
        self.selectedSchoolId = schoolSelectionItem.schoolId
        self.selectedGradeId = schoolSelectionItem.gradeId
        self.selectedLocation = schoolSelectionItem.location

    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("yo")
        clickedTextField = textField
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
    
    
    @objc func keyboardWillShow(sender: NSNotification,_ textField : UITextField) {
        if let keyboardSize = (sender.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            print(clickedTextField.frame.maxY)
            print(keyboardSize.origin.y)
            if clickedTextField.frame.maxY > keyboardSize.origin.y {
                self.view.frame.origin.y = keyboardSize.origin.y - clickedTextField.center.y - 20
            }
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    @objc func selectImage(_ sender: UITapGestureRecognizer){
        print("selecting Image")
        
        let alert = UIAlertController(title: "Select image", message: "Your Profile Image", preferredStyle: .alert)
        let camAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.getPhoto(photoOrCamera: true)
        }
        
        let photoLib = UIAlertAction(title: "Photo Library", style: .default) { (action) in
            self.getPhoto(photoOrCamera: false)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (action) in

        }
        
        alert.addAction(camAction)
        alert.addAction(photoLib)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let chosenImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            UserProfileImageView.contentMode = .scaleAspectFit
            UserProfileImageView.image = chosenImage
            self.dismiss(animated: true, completion: nil)
            
        }else if let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            UserProfileImageView.image = chosenImage
        } else{
            
            print("Something went wrong")
            
        }
    }

    
    func getPhoto(photoOrCamera:Bool){
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            if photoOrCamera {
                imagePicker.sourceType = .camera
                imagePicker.allowsEditing = true
            }else{
                imagePicker.sourceType = .photoLibrary
                imagePicker.allowsEditing = true
            }
            
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        
        
    }
    
//    func hasSpecialCharacters() -> Bool {
//
//        do {
//            let regex = try NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: .caseInsensitive)
//            if let _ = regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, self.count)) {
//                return true
//            }
//
//        } catch {
//            debugPrint(error.localizedDescription)
//            return false
//        }
//
//        return false
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 25
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    
    
    func validations() -> Bool {
         if let firstname = FirstNameTextField.text,let lastname = LastNameTextField.text {
            
            //special characters validations
            let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ")
            if firstname.rangeOfCharacter(from: characterset.inverted) != nil || lastname.rangeOfCharacter(from: characterset.inverted) != nil {
                print("string contains special characters")
                errorAlert(title: "", message: "Name should not contain special characters", vc: self)
                return false
            }
            
            //max 25 char validation
            if firstname.count > 25 {
                errorAlert(title: "", message: "Name lenght should be less than 25 characters.", vc: self)
                return false
            }
            
            //empty string validation
//            if firstname == "" || lastname == ""{
//                return false
//            }
            
            if firstname == ""{
                errorAlert(title: "", message: "Full Name is a required field", vc: self)
                return false
            }
            
            if (UserProfileImageView.image?.isEqual(to: UIImage(named: "owl")!))! {
                print("image matched.")
                UserProfileImageView.image = UIImage()
                return true
            }
                
//            if UserProfileImageView.image == UIImage(named: "owl") {
//                print("image matched.")
//                UserProfileImageView.image = UIImage()
//                return true
//            }
            
            else{
                return true
            }
        }
        return false
    }
    
    
    func registerUser(){
        
        if let firstname = FirstNameTextField.text,let lastname = LastNameTextField.text,let image = UserProfileImageView.image {
            print(image)
//            userItem = UserItem(name: firstname, schoolId: self.selectedSchoolId, gradeId: self.selectedGradeId, location: self.selectedLocation, image: image, fcm: fcmtoken)
//
//            performSegue(withIdentifier: "mobreg", sender: nil)
            registrationModel.registerUserWith(firstName: firstname, lastName: lastname, schoolId: self.selectedSchoolId,gradeId : self.selectedGradeId, location: self.selectedLocation,userImage: image, token: fcmtoken) { (bool) in
                if bool {
                    print("completion success")
                    self.activityIndicator.stopAnimating()

                    self.performSegue(withIdentifier: "home", sender: nil)

                }else{
                    self.activityIndicator.stopAnimating()
                    errorAlert(title: "", message: "server error", vc: self)
                    self.DoneButton.isEnabled = true
                    print("registration error")
                }

            }
            
        }
        
    }
    
    @IBAction func onDoneButtonTapped(_ sender: UIButton) {
        DoneButton.isEnabled = false
        
//        activityIndicator.startAnimating()
        
        if validations() {
            registerUser()
//            performSegue(withIdentifier: "mobreg", sender: nil)
        }else{
            print("Validation error")
//            errorAlert(title: "", message: "First and Last Name are required fields", vc: self)
            DoneButton.isEnabled = true
//            activityIndicator.stopAnimating()
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
//        if segue.identifier == "mobreg" {
//
//            if let vc = segue.destination as? EnterMobileNoViewController {
//
//                vc.userItem = self.userItem
//
//
//
//            }
//
//
//        }
        if segue.identifier == "home" {
            if let Tvc = segue.destination as? UITabBarController {
                //tab 1
                if let Nvc = Tvc.viewControllers!.first as? UINavigationController {
                    if let vc = Nvc.viewControllers.first as? NewActivityViewController {
                        vc.activityModel = MainActivityModel.sharedInstance
                        vc.TempSchoolSelectionModel = SchoolSelectionModel.sharedInstance
                        vc.analysisModel = AnalysisViewModel.sharedInstance

                    }
                }
                //tab 2
                if let Nvc = Tvc.viewControllers![1] as? UINavigationController {
                    if let vc = Nvc.viewControllers.first as? TimelineViewController {
                        vc.timelineModel = TimelineViewModel()

                    }
                }

                //tab3
                if let Nvc = Tvc.viewControllers![2] as? UINavigationController {
                    if let vc = Nvc.viewControllers.first as? AnalysisViewController {
                        //                        vc.timelineModel = TimelineModel()


                        vc.analysisModel = AnalysisViewModel.sharedInstance

                    }
                }

                //tab 4
                if let Nvc = Tvc.viewControllers![3] as? UINavigationController {
                    if let vc = Nvc.viewControllers.first as? MyProfileViewController {
                        //                        vc.timelineModel = TimelineModel()


                        vc.myProfileModel = MyProfileViewModel()

                    }
                }


            }
        }
    }
    

}


extension UIImage {
    func isEqual(to image: UIImage) -> Bool {
        guard let data1: Data = self.pngData(),
            let data2: Data = image.pngData() else {
                return false
        }
        return data1.elementsEqual(data2)
    }
}

//
//extension RegistrationViewController : RegisterProtocol {
//
//    func onResponse(flag: Bool) {
//        if flag {
//            performSegue(withIdentifier: Constants.PerformSegue.homeSegue, sender: nil)
//        }else{
//            print("registration error")
//        }
//    }
//
//
//}

