//
//  RegistrationModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 05/04/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import UIKit
import  Alamofire

//protocol RegisterProtocol : class {
//    func onResponse(flag : Bool)
//}

class RegistrationModel {
    
    static let sharedInstance = RegistrationModel()
    
    
    init() {
        
    }
//    weak var delegate : RegisterProtocol?
//
//    func setDelegate(delegate: RegisterProtocol){
//        self.delegate = delegate
//    }
    var userImageName : String!
    var registerCompletionHandler : ((Bool) -> ())? = nil

    


    
    func registerUserWith(firstName: String,lastName: String,schoolId : Int,gradeId : Int,location: String,userImage : UIImage,token : String, completion :@escaping (Bool) -> Void) {
        
        registerCompletionHandler = completion
        
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.userupdate
        let packetCreator = Packets()
        let userPacket = packetCreator.createRegistrationPacketWith(first_name: firstName, last_name: lastName, schoolId: schoolId, location: location, grade: gradeId, fcmtoken: token)
        
        NetworkModel.sharedInstance.startNetworkImagetCallWith(URL: url, method: .post, imageParamName: NetworkConstant.UserParams.image, image: userImage, andParams: userPacket, onCompletion: onCompletion)
        
    }
    
    func onCompletion(encodingResult: SessionManager.MultipartFormDataEncodingResult){
        
        
        switch encodingResult {
        case .success(let upload, _, _):
            upload.responseJSON { (response) in
                print(response.result.value)
                if let data = response.data{
                    
                    do{
                        
                        let registerResponse = try JSONDecoder().decode(RegisterModel.self, from: data)
                        if self.saveDetailsToDb(register: registerResponse) {
                            
                            //                            self.delegate?.onResponse(flag: true)
                            
                            if let callback = self.registerCompletionHandler{
                                callback(true)
                            }
                            
                        }else{
                            //                            self.delegate?.onResponse(flag: false)
                            if let callback = self.registerCompletionHandler{
                                callback(false)
                            }
                        }
                        
                    }catch{
                        print("unable to parse")
                        if let callback = self.registerCompletionHandler{
                            callback(false)
                        }
                        //                        self.delegate?.onResponse(flag: false)
                    }

                }
            }
            
        case .failure(let error):
            print("\(error)")
            
            break
        default:
            print("error")
        }
        
        
    }
    
    func saveDetailsToDb(register : RegisterModel) -> Bool{
        
        let userModel = parseToUserDetailsModel(register : register)
        
        let realm = AppSharedDetails.getRealmInstance()
        do{
            
            try realm.write {
                realm.add(userModel)
            }
            
        }catch{
            print("unable to save in db")
            return false
        }
        
//        let appuser = AppUserDetails()
//        appuser.setUser(id: register.id)
//        appuser.setJWT(token: register.jwtToken)
        
        return true
        
    }
    
    func parseToUserDetailsModel(register : RegisterModel) -> UserModel{
        
        let userModel = UserModel()
        userModel.first_name = register.firstName
//        userModel.last_name = register.lastName
        userModel.location = register.location
        userModel.schoolid = register.schoolId
        userModel.school = register.schoolName
        userModel.gradeid = register.gradeId
        userModel.grade = register.gradeName
        
        
        if let image = register.image {
           userModel.userImagePath = image
        }else{
            if let imgUrl = register.image_url {
                userModel.userImagePath = imgUrl
            }
            
        }
        
        let user = AppUserDetails()
        user.setUser(id: register.id)
//        user.setUserUpdate(userId: register.id)
        
        
        return userModel
        
        
        
    }

    

    //design here
    func setup(textFields : [UITextField]){
      
            for field in textFields {
                
                field.borderStyle = .none
                let borderLine = UIView()
                let height = 1.0
                borderLine.frame = CGRect(x: 0, y: Double(field.frame.height) - height, width: Double(field.frame.width), height: height)
                
//                field.layer.cornerRadius = 5
//                field.layer.masksToBounds = true
                borderLine.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                borderLine.autoresizingMask = .flexibleWidth
                field.addSubview(borderLine)
 
            }
    
    }
    
    func setupImageView(view : UIView){
        
        view.layer.cornerRadius = 8
        view.layer.masksToBounds = true
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor(hexString: "#FDE891").cgColor
        
    }
}

