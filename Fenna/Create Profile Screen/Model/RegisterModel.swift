//
//  RegisterModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 09/04/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation


    struct RegisterModel : Codable{
        var id : String
        var firstName : String
        var lastName : String
        var location : String
        var schoolId : Int
        var gradeId : Int
        var gradeName : String
        var image : String?
        var image_url : String?
        var schoolName : String
//        var schoolid : String
//        var jwtToken : String
        
        enum CodingKeys : String,CodingKey {
            case id
            case firstName = "first_name"
            case lastName = "last_name"
            case location = "location"
            case schoolId = "school"
            case gradeId = "grade"
            case image = "image"
            case image_url = "image_url"
            case schoolName = "school_name"
            case gradeName = "grade_name"
//            case schoolid = "school_id"
//            case jwtToken = "jwt_token"
        }
    }
    

