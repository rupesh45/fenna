//
//  SettingsModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 11/04/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

class MyProfileViewModel {
    

    init() {
        
    }
    
    var registerCompletionHandler : ((Bool) -> ())? = nil
    
    func UpdateProfileImage(userId : String,image : UIImage,completion : @escaping(Bool) -> Void){
        
        registerCompletionHandler = completion
        
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.users + "\(userId)/"
        
        NetworkModel.sharedInstance.startNetworkImagetCallWith(URL: url, method: .patch, imageParamName: NetworkConstant.UserParams.image, image: image, andParams: [:], onCompletion: onCompletion)
        
    
        
    }
    
    func onCompletion(encodingResult: SessionManager.MultipartFormDataEncodingResult){
        
        switch encodingResult {
        case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
            upload.responseJSON { (response) in
                
                print(response.result.value)
                if let data = response.data {
                    
                    do{
                        
                        let patchres = try JSONDecoder().decode(MyProfileModel.UserDataPatchRes.self, from: data)
                        
                        let realm = AppSharedDetails.getRealmInstance()
                        let userData = realm.objects(UserModel.self)
                        
                        
                        do{
                            
                            try realm.write {
                                if let newImg = patchres.image {
                                    userData[0].userImagePath = newImg
                                }
                                
                            }
                            
                            if let callback = self.registerCompletionHandler{
                                callback(true)
                            }
                            
                        }catch{
                            print("unable to save in db")
                            if let callback = self.registerCompletionHandler{
                                callback(false)
                            }
                        }
                        
                        
                    }catch{
                        print("unable to parse")
                        if let callback = self.registerCompletionHandler{
                            callback(false)
                        }
                    }
                    
                    
                }
            }
            
        case .failure(let err):
            print(err)
            if let callback = self.registerCompletionHandler{
                callback(false)
            }
        default:
            if let callback = self.registerCompletionHandler{
                callback(false)
            }
        }
        
        
        
        
    }
    
    func GetUserImage(withUrl :String, completion : @escaping(UIImage) -> Void ){
        var studentImage = UIImage()
        Alamofire.request(withUrl).responseImage { response in
  
            if let image = response.result.value {
                print("image downloaded: \(image)")
                studentImage = image
                completion(studentImage)
            }
            
        }
        
    }
    
    //design here
    func setupButtons(buttons : [UIButton]){
        for button in buttons {
            button.layer.cornerRadius = button.frame.width / 2
            button.layer.masksToBounds = true
            button.addGradientBackground(firstColor: UIColor(hexString: "#363DDB"), secondColor: UIColor(hexString: "#3430C2"))
        }
    }
    
    func setupImageView(image : UIImageView) {
        image.layer.cornerRadius = image.frame.width / 2
        image.layer.masksToBounds = true
        image.layer.borderWidth = 3
        image.layer.borderColor = UIColor(hexString: "#FDE891").cgColor
    }
    

    
    
    
}
