//
//  SettingsViewController.swift
//  Fenna
//
//  Created by Rupesh Kadam on 18/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
//import Kingfisher
import AlamofireImage

class MyProfileViewController: BackGroundViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var StudentImageView: UIImageView!
    @IBOutlet weak var StudentNameLabel: UILabel!
    @IBOutlet weak var StudentSchoolNameLabel: UILabel!
    @IBOutlet weak var StudentGradeNameLabel: UILabel!
    @IBOutlet weak var SettingLabel1: UILabel!
    @IBOutlet weak var SettingLabel2: UILabel!
    @IBOutlet weak var SettingLabel3: UILabel!
    @IBOutlet weak var EditButton: UIButton!
    
    @IBOutlet weak var MyBadgesCollectionView: UICollectionView!
    
    var AllButtons = [UIButton]()
    var userData : Results<UserModel>!
    let realm = AppSharedDetails.getRealmInstance()
    var myProfileModel : MyProfileViewModel!
    var imagePicker = UIImagePickerController()
    
    var userid = AppUserDetails.getUserId()
    var userProfileModel : UserProfileViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userData = realm.objects(UserModel.self)
        myProfileModel.setupImageView(image: StudentImageView)
        setupSettingsScreen()
        
        MyBadgesCollectionView.delegate = self
        MyBadgesCollectionView.dataSource = self
        
        
        userProfileModel = UserProfileViewModel()
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func setupSettingsScreen(){
        
        setTap()
        
        let url = userData[0].userImagePath
        print(url)
        let userFirstName = userData[0].first_name
        let userSchoolName = userData[0].school
        let userGradeName = userData[0].grade
        
//        myProfileModel.GetUserImage(withUrl: url) { (image) in
//            self.StudentImageView.image = image
//        }
        
        StudentImageView.loadImageFromurl(urlString: url)
        
        StudentNameLabel.text = userFirstName.capitalizingFirstLetter()
        StudentSchoolNameLabel.text = userSchoolName
        StudentGradeNameLabel.text = userGradeName
    }
    
    func setTap() {
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(OnStudentImageTapped(_:)))
        StudentImageView.addGestureRecognizer(tap)
        imagePicker.delegate = self
        
    }
    
    @objc func OnStudentImageTapped(_ sender : UITapGestureRecognizer) {
        
        print("image tapped.")
        let alert = UIAlertController(title: "Select image", message: "Your Profile Image", preferredStyle: .alert)
        let camAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.getPhoto(photoOrCamera: true)
        }
        
        let photoLib = UIAlertAction(title: "Photo Library", style: .default) { (action) in
            self.getPhoto(photoOrCamera: false)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (action) in
            
        }
        
        alert.addAction(camAction)
        alert.addAction(photoLib)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    func getPhoto(photoOrCamera:Bool){
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            if photoOrCamera {
                imagePicker.sourceType = .camera
                imagePicker.allowsEditing = true
            }else{
                imagePicker.sourceType = .photoLibrary
                imagePicker.allowsEditing = true
            }
            
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let chosenImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            //patch call
            
            myProfileModel.UpdateProfileImage(userId: userid, image: chosenImage) { (complete) in
                if complete {
                    
                    self.StudentImageView.contentMode = .scaleAspectFit
                    self.StudentImageView.image = chosenImage
                    self.dismiss(animated: true, completion: nil)
                    
                    //timeline refresh notification
                    NotificationCenter.default.post(name: Notification.Name.refreshTimeline, object: nil)
                    
                }else{
                    self.dismiss(animated: true, completion: nil)
                    print("response error")
                }
            }
            
            
            
        }else if let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            //patch call
            myProfileModel.UpdateProfileImage(userId: userid, image: chosenImage) { (complete) in
                if complete {
                    self.StudentImageView.contentMode = .scaleAspectFit
                    self.StudentImageView.image = chosenImage
//                    self.dismiss(animated: true, completion: nil)
                }else{
//                    self.dismiss(animated: true, completion: nil)
                    print("response error")
                }
            }
        } else{
            print("Something went wrong")
            
        }
    }
    

    @IBAction func OnEditButtonTapped(_ sender: UIButton) {

        
        
        if StudentImageView.isUserInteractionEnabled == false {
            sender.setTitle("Cancel", for: .normal)
            
        }else{
            sender.setTitle("Update profile picture.", for: .normal)
        }
        
        StudentImageView.isUserInteractionEnabled = !StudentImageView.isUserInteractionEnabled
        

    }

}

extension MyProfileViewController : UICollectionViewDelegate,UICollectionViewDataSource {


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userProfileModel.BadgeImages.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! BadgesCollectionViewCell

        cell.userProModel = userProfileModel
        cell.BadgeImageView.image =  userProfileModel.getEmptyBadgeImages(index: indexPath.row)
        cell.BadgeImageView.alpha = 0.3

        if !cell.heightSet {
            userProfileModel.increaseImageHeight(imageView: cell.BadgeImageView)
            cell.heightSet = true
        }


        if userProfileModel.getBadgesCount() >= indexPath.row + 1{

            cell.BadgeImageView.image = userProfileModel.getBadgesAt(index : indexPath.row)
            cell.BadgeImageView.alpha = 1

        }else{


        }


        return cell
    }





}
