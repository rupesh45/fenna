//
//  ParametersConstant.swift
//  Fenna
//
//  Created by Rupesh Kadam on 29/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation

class ParameConstants {
    
    init() {
        
    }
    
    struct RegistrtationParamKey {
        static let first_name = "first_name"
        static let last_name = "last_name"
        static let school = "school"
        static let location = "location"
        static let grade = "grade"
        static let fcm_token = "fcm_token"
    }
    
    struct updateUserParamKey {
        static let school = "school"
        static let grade = "grade"
        static let loacation = "location"
    }
    
    struct StartActParamKey {
        static let activity_id = "activity"
        static let duration = "duration"
    }
    
    struct EndActParamKey {
        static let pre_endtime = "pre_endtime"
        static let pre_duration = "pre_duration"
    }
    
    struct FollowerParamKey {
        static let follower = "follower"
    }
    
    struct LikeParamKey {
        static let user = "user"
        static let activity = "activity"
    }
    
//    struct MobileVerificationKey {
//        static let phone_number = "phone_number"
//    }
    
}
