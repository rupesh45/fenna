//
//  Parameters.swift
//  Fenna
//
//  Created by Rupesh Kadam on 29/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class Packets {
    
    init() {
        
    }
    
    func createRegistrationPacketWith(first_name: String,last_name: String,schoolId: Int,location: String,grade: Int,fcmtoken : String)-> Parameters{
        let parameters : Parameters = [ParameConstants.RegistrtationParamKey.first_name : first_name,
                                       ParameConstants.RegistrtationParamKey.last_name : last_name,
                                       ParameConstants.RegistrtationParamKey.school : schoolId,
                                       ParameConstants.RegistrtationParamKey.location : location,
                                       ParameConstants.RegistrtationParamKey.grade : grade,
                                       ParameConstants.RegistrtationParamKey.fcm_token :  fcmtoken]
        return parameters
        
    }
    
    func createUserUpdatePacketWith(location : String,schoolId : Int,gradeId : Int) -> Parameters {
        
        let param : Parameters = [ParameConstants.updateUserParamKey.loacation : location,
                                  ParameConstants.updateUserParamKey.school : schoolId,
                                  ParameConstants.updateUserParamKey.grade : gradeId]
        
        return param
        
    }
    
    func createStartActivityPacketWith(Activity_id : Int,duration : Int) -> Parameters{
        let parameters : Parameters = [ParameConstants.StartActParamKey.activity_id : Activity_id,
                                       ParameConstants.StartActParamKey.duration : duration]
        return parameters
    }
    
    func createEndActivityPacketWith(pre_duration : Int) -> Parameters {
//        let parameters : Parameters = [ParameConstants.EndActParamKey.pre_endtime : pre_endtime,
//                                       ParameConstants.EndActParamKey.pre_duration : pre_duration]
        
        let parameters : Parameters = [ParameConstants.EndActParamKey.pre_duration : pre_duration]
        
        return parameters
    }
    
    func createFollowerPacketWith(followerId : String) -> Parameters{
        let parameters : Parameters = [ParameConstants.FollowerParamKey.follower : followerId]
        return parameters
    }
    
    func createLikePacketWith(activityId : Int,userId : String) -> Parameters {
        let parametrs : Parameters = [ParameConstants.LikeParamKey.user : userId,
                                      ParameConstants.LikeParamKey.activity : activityId ]
        return parametrs
    }
    
    
}
