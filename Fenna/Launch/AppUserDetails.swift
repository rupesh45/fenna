//
//  AppUserDetails.swift
//  Fenna
//
//  Created by Rupesh Kadam on 05/04/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import UIKit

class AppUserDetails: NSObject {
    
    //get
    static func getUserId() -> String{
        let defaults = AppSharedDetails.getUserDefaults()
        if let userId = defaults.string(forKey: Constants.UserDefaultKeys.userId)
        {
            return userId
        }
        
        return ""
    }
    
    static func getPreviousUserId() -> String{
        let defaults = AppSharedDetails.getUserDefaults()
        if let userId = defaults.string(forKey: Constants.UserDefaultKeys.previousUserId)
        {
            return userId
        }
        
        return ""
    }
    
    static func getJWTToken() -> String{
        let defaults = AppSharedDetails.getUserDefaults()
        if let jwtToken = defaults.string(forKey: Constants.UserDefaultKeys.jwtToken)
        {
            return jwtToken
        }
        
        return ""
    }
    
    static func getFcmToken() -> String {
        let defaults = AppSharedDetails.getUserDefaults()
        if let fcmtoken = defaults.string(forKey: Constants.UserDefaultKeys.fcmToken)
        {
            return fcmtoken
        }
        return ""
    }
    
    static func getMobileNumber() -> String{
        
        let defaults = AppSharedDetails.getUserDefaults()
        if let mobileNumber = defaults.string(forKey: Constants.UserDefaultKeys.mobileNumber)
            
        {
            return mobileNumber
        }
        return ""
    }
    
    //its has same userId
    static func getUserUpdateId() -> String {
        
        let defaults = AppSharedDetails.getUserDefaults()
        if let userupdate = defaults.string(forKey: Constants.UserDefaultKeys.userupdate)
            
        {
            return userupdate
        }
        return ""
        
    }
    
    //set
    func setUser(id : String)
    {
        let defaults = AppSharedDetails.getUserDefaults()
        defaults.set(id, forKey: Constants.UserDefaultKeys.userId)
        defaults.synchronize()
    }
    
    func setPreviousUser(id : String)
    {
        let defaults = AppSharedDetails.getUserDefaults()
        defaults.set(id, forKey: Constants.UserDefaultKeys.previousUserId)
        defaults.synchronize()
        
    }
    
    
    func setJWT(token : String)
    {
        let defaults = AppSharedDetails.getUserDefaults()
        defaults.set(token, forKey: Constants.UserDefaultKeys.jwtToken)
        defaults.synchronize()
    }
    
    func setFcmToken(token : String)
    {
        let defaults = AppSharedDetails.getUserDefaults()
        defaults.set(token, forKey: Constants.UserDefaultKeys.fcmToken)
        defaults.synchronize()
    }
    
    func setMobileVerified(mobile : String)
    {
        let defaults = AppSharedDetails.getUserDefaults()
        defaults.set(mobile, forKey: Constants.UserDefaultKeys.mobileNumber)
        defaults.synchronize()
    }
    
    func setUserUpdate(userId : String)
    {
        let defaults = AppSharedDetails.getUserDefaults()
        defaults.set(userId, forKey: Constants.UserDefaultKeys.userupdate)
        defaults.synchronize()
    }
}
