//
//  AppSharedDetail.swift
//  Fenna
//
//  Created by Rupesh Kadam on 05/04/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import Realm

class AppSharedDetails: NSObject {
    
    static let sharedDefaults = UserDefaults.standard
    
    static let realmVersion : UInt64 = 2
    //static let realmUrl = getSharedFileUrl().appendingPathComponent(realmDBPath)
    static var realmConfig = Realm.Configuration(schemaVersion: realmVersion, migrationBlock: { (migration, oldSchemaVersion) in
        if (oldSchemaVersion < realmVersion) {
            
            
        }})

    static func getUserDefaults() -> UserDefaults
    {
        return sharedDefaults
    }
    
    static func getRealmInstance() -> Realm
    {
        
        return try! Realm(configuration: realmConfig)
    }
    

    
}
