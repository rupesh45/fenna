//
//  SplashScreenViewController.swift
//  Fenna
//
//  Created by Rupesh Kadam on 20/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController {
    

    @IBOutlet weak var FennaLogoImageView: UIImageView!
    
    var splashViewModel : SplashModel!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()


        // Do any additional setup after loading the view.
        FennaLogoImageView.layer.cornerRadius = FennaLogoImageView.frame.width / 2
        FennaLogoImageView.layer.masksToBounds = true
        
        splashViewModel = SplashModel()
        splashViewModel.getUserDetails()
        splashViewModel.getMobileNoDetails()
//        splashViewModel.getUserUpdateDetails()
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.PerformSegue.homeSegue {
            
//            UserDefaults.standard.setValue(true, forKey: "true")
            
            if let Tvc = segue.destination as? UITabBarController {
                //tab 1
                if let Nvc = Tvc.viewControllers!.first as? UINavigationController {
                    if let vc = Nvc.viewControllers.first as? NewActivityViewController {
                        vc.activityModel = MainActivityModel.sharedInstance
                        vc.TempSchoolSelectionModel = SchoolSelectionModel.sharedInstance
                        vc.analysisModel = AnalysisViewModel.sharedInstance
                        
                        
                        
                    }
                }
                //tab 2
                if let Nvc = Tvc.viewControllers![1] as? UINavigationController {
                    if let vc = Nvc.viewControllers.first as? TimelineViewController {
                        vc.timelineModel = TimelineViewModel()
                    }
                }
                
                //tab3
                if let Nvc = Tvc.viewControllers![2] as? UINavigationController {
                    if let vc = Nvc.viewControllers.first as? AnalysisViewController {
                        //                        vc.timelineModel = TimelineModel()
          
                        
                       vc.analysisModel = AnalysisViewModel.sharedInstance
                        
                    }
                }
                
                //tab 4
                if let Nvc = Tvc.viewControllers![3] as? UINavigationController {
                    if let vc = Nvc.viewControllers.first as? MyProfileViewController {
                        //                        vc.timelineModel = TimelineModel()
                        
                        
                    vc.myProfileModel = MyProfileViewModel()
                        
                    }
                }
                
                
            }
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if splashViewModel.getUserRegistrationStatus() && splashViewModel.getMobileRegistrationStatus() {
            //Display main
            performSegue(withIdentifier: Constants.PerformSegue.homeSegue, sender: nil)
            
        }
//        else if splashViewModel.getUserRegistartionStatus(){
//
//            performSegue(withIdentifier: Constants.PerformSegue.loginSegue, sender: nil)
//
//        }
//        else if splashViewModel.getMobileRegistrationStatus(){
//            //Display mbo register
//            performSegue(withIdentifier: Constants.PerformSegue.loginSegue, sender: nil)
//        }
        else{
            //display register
            performSegue(withIdentifier: Constants.PerformSegue.loginSegue, sender: nil)
        }
        
        
        
    }

}
