//
//  SchoolSelectionViewController.swift
//  Fenna
//
//  Created by Rupesh Kadam on 11/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit
import iOSDropDown
import NVActivityIndicatorView

struct SchoolSelectionItem {
    let location : String
    let schoolId : Int
    let gradeId : Int
}

class SchoolSelectionViewController: UIViewController {
    
    @IBOutlet weak var LocationDropdown: DropDown!
    @IBOutlet weak var SchoolDropdown: DropDown!
    @IBOutlet weak var GradeDropdown: DropDown!
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    
    
    var schoolSelectionModel : SchoolSelectionModel!
    var schoolId : Int!
    var gradeId : Int!
    
    var dds = [DropDown]()
    var clickedTextField = DropDown()

    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.setupActivityIndicator()
        
        
        setDropdownDelegate()
        GradeDropdown.isSearchEnable = false
        GradeDropdown.arrowColor = .clear
        schoolSelectionModel = SchoolSelectionModel()
        
        schoolSelectionModel.SetupDesign(view: self.view, button: NextButton)
        
        activityIndicator.startAnimating()
        
        schoolSelectionModel.getSchoolInfo { (bool) in
            if bool {
                self.activityIndicator.stopAnimating()
                self.SelectYourSchool()
            }else{
                self.activityIndicator.stopAnimating()
                print("response error")
            }
        }
        
        
    }
    
    func setDropdownDelegate(){
        dds = [LocationDropdown,SchoolDropdown]
        for d in dds{
            d.isSearchEnable = true
            d.arrowColor = .clear
//            d.delegate = self
        }
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        print(self.view.frame.width)
        schoolSelectionModel.SetupDropdown(dropdown: [LocationDropdown,SchoolDropdown,GradeDropdown],view : self.view)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mobreg" {

            if let vc = segue.destination as? EnterMobileNoViewController {
                let schoolSelectionItem = SchoolSelectionItem(location: self.LocationDropdown.text!, schoolId: self.schoolId, gradeId: self.gradeId)
                
                vc.schoolSelectionItem = schoolSelectionItem
//                vc.selectedSchool = SchoolDropdown.text
//                vc.selectedLocation = LocationDropdown.text
//                vc.selectedGrade = GradeDropdown.text
//                vc.selectedSchoolId = schoolId
//                vc.selectedGradeId = gradeId
//                print(schoolId)
//                print(gradeId)
            }

        }
    }
    
    func SelectYourSchool(){
        
        LocationDropdown.optionArray = schoolSelectionModel.states
        LocationDropdown.text = schoolSelectionModel.states.first
        LocationDropdown.didSelect {[weak self] (selectedText, index, id) in
            self!.LocationDropdown.text = selectedText

        }
                
        
        SchoolDropdown.optionArray = schoolSelectionModel.getSchoolNames()
        print(schoolSelectionModel.getSchoolNames())
        SchoolDropdown.didSelect {[weak self] (selectedText, index, id) in
            self!.SchoolDropdown.text = selectedText
            self!.schoolId = self!.schoolSelectionModel.getSchoolId(index: index)
            print("schoolId",self!.schoolId)
            print("schoolname",selectedText)
//            self?.gradeId = nil
            self?.GradeDropdown.text = ""
            self?.GradeDropdown.optionArray = []
            
            self!.schoolSelectionModel.getGradeArray(index: index)
            
            self!.GradeDropdown.optionArray = self!.schoolSelectionModel.getGradeName()
            self!.GradeDropdown.didSelect {[weak self] (selectedText, index, id) in
                self!.GradeDropdown.text = selectedText
                self!.gradeId = self!.schoolSelectionModel.getGradeId(index : index)
                print("grade id ",self!.gradeId)
                print("grade name ",selectedText)
                
            }
            

        }
        
        
    }
    
    @IBAction func OnNextButtonTapped(_ sender: UIButton) {
//        if LocationDropdown.text == "" || SchoolDropdown.text == "" || GradeDropdown.text == "" {
//            let alert = UIAlertController(title: "Please select the required fields", message: "", preferredStyle: .alert)
//
//            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil ))
//            self.present(alert, animated: true)
//        }else{
//
//            self.performSegue(withIdentifier: "mobreg", sender: nil)
//
////            stayInTheLoopAlert()
//
//
//        }

            if SchoolDropdown.text == "" || GradeDropdown.text == "" {
       
                let alert = UIAlertController(title: "Please select the required fields", message: "", preferredStyle: .alert)

                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil ))
                self.present(alert, animated: true)
                           

                   }else{
                               
                       self.performSegue(withIdentifier: "mobreg", sender: nil)
    
                   }
        
       
    
    }
    
    func stayInTheLoopAlert(){
        let alert = UIAlertController(title: "Stay in the loop", message: "Finna wants to send you updates about activities from other users.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (alert) in
            
            self.pushNotiAlert()
        }))
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert) in
            self.pushNotiAlert()
        }))
        self.present(alert, animated: true)
    }
    
    func pushNotiAlert(){
        let alert = UIAlertController(title: "Push Notifications", message: "Finna wants your permission for push notifications.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (alert) in
            
            self.performSegue(withIdentifier: "registration", sender: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert) in
            self.performSegue(withIdentifier: "registration", sender: nil)
        }))
        self.present(alert, animated: true)
    }
    
    
}
