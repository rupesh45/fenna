//
//  SchoolSelectionModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 11/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import UIKit
import iOSDropDown

class SchoolSelectionModel {
    
    static let sharedInstance = SchoolSelectionModel()
    
    var states = ["California"]
    var schoolsArray = [School]()
    var gradeArray = [Grade]()
    var grade = ["1","2","3","4","5","6","7","8","9","10"]
        
    init() {
        
    }
    
    var selectedSchool : String!
    var selectedLocation : String!
    var selectedGrade : String!
    
    
    func getSchoolCount() -> Int {
        return schoolsArray.count
    }
    
    func getSchoolNames() -> [String] {
        var tempSchoolArray = [String]()
        
        for school in schoolsArray {
            tempSchoolArray.append(school.name)
        }
        
        return tempSchoolArray
    }
    
    func getSchoolId(index : Int) -> Int {
        
        return schoolsArray[index].id
        
        
    }
    
    
    func getGradeArray(index : Int) {
        
        self.gradeArray = schoolsArray[index].grade
        
    }
    
    func getGradeName() -> [String] {
        
        
        
        var tempGradeArray = [String]()
        
        for grade in gradeArray {
            
            tempGradeArray.append(grade.name)
            
        }
        
        return tempGradeArray
        
    }
    
    func getGradeId(index : Int) -> Int {
        
        return gradeArray[index].id
        
    }
    
    
    func getGradeCount() -> Int {
        
        return gradeArray.count
        
    }
    
    func getSchoolInfo(completion : @escaping (Bool) -> Void){
        
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.user + NetworkConstant.SubUrlMethods.schools
        
        NetworkModel.sharedInstance.startNetworkGetCallWith(URL: url) { (response) in
            if let status = response.response?.statusCode {
                switch status {
                case 200...300:
                    print("response success")
                    print(response.result.value)
                    if let data = response.data {
                        
                        do{
                             let schoolData = try JSONDecoder().decode([School].self, from: data)
                            
                            self.schoolsArray = schoolData
                             let realm = AppSharedDetails.getRealmInstance()
                            
                            //save school in realm
                            for school in  self.schoolsArray {
                                
                                let results = realm.objects(SchoolModel.self).filter("id = \(school.id)")
                                
                                if results.count == 0 {
                                    
                                    let schoolObj = SchoolModel()
                                    
                                    schoolObj.id = school.id
                                    schoolObj.name = school.name
                                    
                                    
                                    do {
                                        
                                        try realm.write {
                                            realm.add(schoolObj)
                                        }
                                        
                                    }catch{
                                        print("unable to write in realm")
                                        completion(false)
                                    }
                                    
                                }
                                
                            }
                            print(self.schoolsArray)
                            completion(true)
                                
                        }catch{
                            print("unable to parse")
                            completion(false)
                        }
                       
                        
                    }
                    break
                default:
                    print("unable to parse")
                    completion(false)
                }
            }
        }
        
        
    }
    
    
    
    //controller design from here
    func SetupDropdown(dropdown : [DropDown],view : UIView){
        for d in dropdown {
            d.frame.size = returnDropdownSize(view: view)
            d.backgroundColor = UIColor(hexString: "#304DD5")
            
        }
        
    }
    
    private func returnDropdownSize(view : UIView) -> CGSize {
        return CGSize(width: view.frame.width / 1.2254, height: 40)
    }
    
    func SetupDesign(view : UIView,button : UIButton){
        setGradientBackground(view: view)
        setupButton(button: button)
    }
    
    func SetupDesignBackWithRoundButton(view: UIView,button: UIButton){
        setGradientBackground(view: view)
        setupRoundButton(button: button)
    }
    
    func SetupDesignBack(view : UIView){
        setGradientBackground(view: view)
    }
    
    
    func setGradientBackgroundButton(view : UIView) {
        let colorTop =  UIColor(hexString: "#FBD15B").cgColor
        let colorBottom = UIColor(hexString: "#F7711A").cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = view.bounds
        
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    //private funcs
    private func setupButton(button: UIButton){
        button.layer.cornerRadius = button.frame.size.height / 2
        button.layer.masksToBounds = true
        setGradientBackgroundButton(view: button)
    }
    
    private func setupRoundButton(button: UIButton){
        button.layer.cornerRadius = button.frame.size.width / 2
        button.layer.masksToBounds = true
        setGradientBackgroundButton(view: button)
    }
    
    private func setGradientBackground(view : UIView) {
        let colorTop =  UIColor(hexString: "#2074FC").cgColor
        let colorBottom = UIColor(hexString: "#690ECE").cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = view.bounds
        
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    
    
    
}


extension UIButton {
    
    func setGradientButton() {
        let colorTop =  UIColor(hexString: "#FBD15B").cgColor
        let colorBottom = UIColor(hexString: "#F7711A").cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.bounds
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
}

