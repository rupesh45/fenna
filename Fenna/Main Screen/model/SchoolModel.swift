//
//  SchoolModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 09/04/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation




struct School : Codable {
    var id : Int
    var name : String
    var grade : [Grade]
}

struct Grade : Codable {
    var id : Int
    var name : String
}
