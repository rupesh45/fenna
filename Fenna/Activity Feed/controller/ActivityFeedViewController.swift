//
//  ActivityFeedViewController.swift
//  Fenna
//
//  Created by Rupesh Kadam on 15/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit
import OnlyPictures

class ActivityFeedViewController: BackGroundViewController {

    @IBOutlet weak var ActivityFeedTableView: UITableView!
    
    var timelineModel : TimelineViewModel!
    var activityModel : MainActivityModel!
    var activityFeedViewModel : ActivityFeedViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityFeedViewModel = ActivityFeedViewModel()
        
        activityFeedViewModel.getActivityFeedData { (bool) in
            if bool {
                print("feed success")
                self.ActivityFeedTableView.reloadData()
            }else{
                print("feed response error")
            }
        }
        
        
        ActivityFeedTableView.delegate = self
        ActivityFeedTableView.dataSource = self
        ActivityFeedTableView.registerNib(nibName: "AtivityFeedTableViewCell", identifier: "cell")
    }

}


extension ActivityFeedViewController : UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activityFeedViewModel.FeedData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! AtivityFeedTableViewCell
        cell.tag = indexPath.row
        cell.activityFeedModel = self.activityFeedViewModel
        cell.HorizontalPicturesSet.reloadData()
        
        let url = activityFeedViewModel.FeedData[indexPath.row].activity_url
        cell.ActivityImageView.loadImageFromurl(urlString: url)
        
        return cell
    }
    
    



}

//extension ActivityFeedViewController : OnlyPicturesDelegate,OnlyPicturesDataSource {
//    
//    func pictureView(_ imageView: UIImageView, didSelectAt index: Int) {
//        print("hi")
//    }
//    
//    func numberOfPictures() -> Int {
//        
//        <#code#>
//    }
//    
//    
//    
//    
//}
