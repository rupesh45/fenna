//
//  ActivityFeedModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 15/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage
import Alamofire

class ActivityFeedViewModel {
    
    
    init() {
        
    }
    
    var FeedData : [ActivityFeedModel.FeedResponse] = []
    
    var userImages : [[ActivityFeedModel.FeedResponseUsers]] = []
    
    
    func getActivityFeedData(completion : @escaping(Bool) -> Void){
        
        let url = NetworkConstant.mainURL + NetworkConstant.UrlMethods.activityfilter
        
        NetworkModel.sharedInstance.startNetworkGetCallWithHeader(URL: url) { (response) in
            if let status = response.response?.statusCode {
                print(status)
                print(response.result.value)
                switch status{
                case 200...300:
                    print("response success")
                    print(response.result.value)
                    if let data = response.data {
                        do {
                            let results = try JSONDecoder().decode(ActivityFeedModel.MainFeed.self, from: data)
                            self.FeedData = results.results
                            
                            for images in self.FeedData {
                                self.userImages.append(images.users)
//                                print(images.users)
                            }
                            
//                            print(self.userImages)
        
                            
                            completion(true)
                        }catch {
                            print("unable to parse")
                        }
                        
                        
                    }
                    
                    
                    break
                    
                case 400...600:
                    print("response error")
//                    print(response.result.value!)
                    completion(false)
                    break
                default:
                    print("error")
                    completion(false)
                }
                
                
            }
        }
        
    }
    
    func getActivityName(index: Int) -> String {
        
        return self.FeedData[index].name
        
    }
    
    func activityImage(index : Int,completion : @escaping (UIImage) -> Void){
        
        
    
                let urlString = FeedData[index].activity_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                if  let url = URL(string: urlString){
                    GetActivityImage(withUrl: url) { (image) in
                        completion(image)
                    }
                }
                
   
    }
    
    private func GetActivityImage(withUrl :URL, completion : @escaping(UIImage) -> Void ){
        var activityImage = UIImage()
        Alamofire.request(withUrl).responseImage { (response) in
            if let img = response.result.value {
                activityImage = img
                completion(activityImage)
            }
        }
        
    }
    
    //design
    func ConfigureActivityFeedCell(imageView : UIImageView){
        
        imageView.layer.cornerRadius = imageView.frame.width / 2
        imageView.layer.masksToBounds =  true
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor = UIColor(hexString: "#FDE793").cgColor
        
    }
    
}
