//
//  ActivityFeedModel.swift
//  Fenna
//
//  Created by Rupesh Kadam on 09/05/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import Foundation

class ActivityFeedModel : Codable {
    
    init() {
        
    }
    
    
    struct MainFeed : Codable {
        let results : [FeedResponse]
    }
    
    struct FeedResponse : Codable {
        let id : Int
        let name : String
        let activity_url : String
        let average_time : Int
        let users : [FeedResponseUsers]
    }
    
    struct FeedResponseUsers : Codable {
        let id : Int
        let user_image : String?
        let user_image_url : String?
    }
    
}
