//
//  AtivityFeedTableViewCell.swift
//  Fenna
//
//  Created by Rupesh Kadam on 15/03/19.
//  Copyright © 2019 Analogy Plus. All rights reserved.
//

import UIKit
import OnlyPictures
import SwipeCellKit


class AtivityFeedTableViewCell: SwipeTableViewCell {


    @IBOutlet weak var ActivityImageView: UIImageView!
    @IBOutlet weak var HorizontalPicturesSet: OnlyHorizontalPictures!
    
    @IBOutlet weak var ActivityStatusLabel: UILabel!
    @IBOutlet weak var AverageTimeLabel: UILabel!
    
    @IBOutlet weak var UsersCountLabel: UILabel!
    @IBOutlet weak var ActivityNameLabel: UILabel!
    
    var activityFeedModel : ActivityFeedViewModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        HorizontalPicturesSet.delegate = self
        HorizontalPicturesSet.dataSource = self
        HorizontalPicturesSet.gap = 24
        HorizontalPicturesSet.spacingColor = UIColor(hexString: "#4B63D2")
        HorizontalPicturesSet.alignment = .left
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layoutCell()
        activityFeedModel.ConfigureActivityFeedCell(imageView: ActivityImageView)
        ActivityStatusLabel.sizeToFit()
//        ActivityNameLabel.sizeToFit()
    }
    

    
    
    
    
}


extension AtivityFeedTableViewCell : OnlyPicturesDelegate,OnlyPicturesDataSource {
    
    func pictureView(_ imageView: UIImageView, didSelectAt index: Int) {
        print("hi")
    }
    
    func numberOfPictures() -> Int {
        let feedData =  activityFeedModel.FeedData
        let userSet = activityFeedModel.userImages[self.tag]
        self.ActivityStatusLabel.text = "\(userSet.count)"
        
        self.UsersCountLabel.text = "\(userSet.count)"
        self.ActivityNameLabel.text = activityFeedModel.getActivityName(index: self.tag)
        
        
        self.AverageTimeLabel.text = "Avg Time - \(feedData[self.tag].average_time) min."
        return userSet.count
    }
    
    func visiblePictures() -> Int {
        return 5
    }
    
    func pictureViews(_ imageView: UIImageView, index: Int) {
        
        
        let userSet = activityFeedModel.userImages[self.tag]
        if let imgurl = userSet[index].user_image {
            if let url = URL(string: imgurl){
                
//                imageView.af_setImage(withURL: url)
                imageView.loadImageFromurl(urlString: imgurl)
                
            }
        }
        
        if let imgurlname = userSet[index].user_image_url {
            if let url = URL(string: imgurlname) {
//                imageView.af_setImage(withURL: url)
                imageView.loadImageFromurl(urlString: imgurlname)
            }
        }
        
        
        
    }
    
    
    
}
